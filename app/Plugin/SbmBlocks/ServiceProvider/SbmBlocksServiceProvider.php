<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\SbmBlocks\ServiceProvider;

use Silex\Application as BaseApplication;
use Silex\ServiceProviderInterface;
use Symfony\Component\Yaml\Yaml;

class SbmBlocksServiceProvider implements ServiceProviderInterface
{

    public function register(BaseApplication $app)
    {
        // $app['eccube.plugin.SbmBlocks.repository.SbmBlocks'] = $app->share(function () use ($app) {
        //     return $app['orm.em']->getRepository('Plugin\SbmBlocks\Entity\SbmBlocks');
        // });
        // $app['eccube.plugin.SbmBlocks.repository.SbmBlocksimage'] = $app->share(function () use ($app) {
        //     return $app['orm.em']->getRepository('Plugin\SbmBlocks\Entity\SbmBlocksImage');
        // });

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::index')
        //     ->value('id', null)->assert('id', '\d+|')
        //     ->bind('admin_SbmBlocks');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/new', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::create')
        //     ->value('id', null)->assert('id', '\d+|')
        //     ->bind('admin_SbmBlocks_new');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/commit', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::commit')
        // ->value('id', null)->assert('id', '\d+|')
        // ->bind('admin_SbmBlocks_commit');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/edit/{id}', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::edit')
        //     ->value('id', null)->assert('id', '\d+|')
        //     ->bind('admin_SbmBlocks_edit');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/delete/{id}', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::delete')
        // ->value('id', null)->assert('id', '\d+|')
        // ->bind('admin_SbmBlocks_delete');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/rank_up/{id}', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::rankUp')
        //     ->value('id', null)->assert('id', '\d+|')
        //     ->bind('admin_SbmBlocks_rank_up');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/rank_down/{id}', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::rankDown')
        //     ->value('id', null)->assert('id', '\d+|')
        //     ->bind('admin_SbmBlocks_rank_down');

        // $app->match('/' . $app["config"]["admin_route"] . '/SbmBlocks/addimage/{id}', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksController::addImage')
        //     ->value('id', null)->assert('id', '\d+|')
        //     ->bind('admin_SbmBlocks_image_add');

        // // 商品検索画面表示
        // $app->post('/' . $app["config"]["admin_route"] . '/SbmBlocks/search/userpage', '\Plugin\SbmBlocks\Controller\Admin\SbmBlocksSearchModelController::searchUserPage')
        //     ->bind('admin_SbmBlocks_search_userpage');

        // ブロック
        // twigファイルを全て取得
        $files = glob(__DIR__ . '/../Resource/template/Block/*.twig');
        $list = array();
        foreach ($files as $file) {
            if (is_file($file)) {
                $list[] = str_replace(__DIR__ . '/../Resource/template/Block/','',str_replace('.twig','',$file));
            }
            // 再帰的にする場合
            // if (is_dir($file)) {
            //     $this->originBlockList = array_merge($this->originBlockList, getFileList($file));
            // }
        }
        // dump($list);exit;

        foreach ($list as $key => $val) {
            $app->match('/block/'.$val, '\Plugin\SbmBlocks\Controller\SbmBlocksController::index')
                ->bind('block_'.$val);
        }


        // $app->match('/block/SbmBlocks_block/options', '\Plugin\SbmBlocks\Controller\Block\SbmBlocksController::index')
        //     ->value('listtype','options')
        //     ->bind('block_customurl_userpage_options_block');

        // $app->match('/block/SbmBlocks_block1/campaign', '\Plugin\SbmBlocks\Controller\Block\SbmBlocksController::index')
        //     ->value('listtype','campaign')
        //     ->value('block','block1')
        //     ->bind('block_customurl_userpage_campaign_block1');

        // $app->match('/block/SbmBlocks_block2/campaign', '\Plugin\SbmBlocks\Controller\Block\SbmBlocksController::index')
        //     ->value('listtype','campaign')
        //     ->value('block','block2')
        //     ->bind('block_customurl_userpage_campaign_block2');

        // $app->match('/block/SbmBlocks_block3/campaign', '\Plugin\SbmBlocks\Controller\Block\SbmBlocksController::index')
        //     ->value('listtype','campaign')
        //     ->value('block','block3')
        //     ->bind('block_customurl_userpage_campaign_block3');

        // $app->match('/block/SbmBlocks_block4/campaign', '\Plugin\SbmBlocks\Controller\Block\SbmBlocksController::index')
        //     ->value('listtype','campaign')
        //     ->value('block','block4')
        //     ->bind('block_customurl_userpage_campaign_block4');

        // $app->match('/block/SbmBlocks_block5/campaign', '\Plugin\SbmBlocks\Controller\Block\SbmBlocksController::index')
        //     ->value('listtype','campaign')
        //     ->value('block','block5')            
        //     ->bind('block_customurl_userpage_campaign_block5');


        // // 型登録
        // $app['form.types'] = $app->share($app->extend('form.types', function ($types) use ($app) {
        //     $types[] = new \Plugin\SbmBlocks\Form\Type\SbmBlocksType($app);
        //     $types[] = new \Plugin\SbmBlocks\Form\Type\Admin\SearchPageLayoutType($app);
        //     return $types;
        // }));

        // // サービスの登録
        // $app['eccube.plugin.SbmBlocks.service.SbmBlocks'] = $app->share(function () use ($app) {
        //     return new \Plugin\SbmBlocks\Service\SbmBlocksService($app);
        // });

        // // メッセージ登録
        // $app['translator'] = $app->share($app->extend('translator', function ($translator, \Silex\Application $app) {
        //     $translator->addLoader('yaml', new \Symfony\Component\Translation\Loader\YamlFileLoader());

        //     $file = __DIR__ . '/../Resource/locale/message.' . $app['locale'] . '.yml';
        //     if (file_exists($file)) {
        //         $translator->addResource('yaml', $file, $app['locale']);
        //     }

        //     return $translator;
        // }));

        // //カスタムURLの定義反映
        // $this->bind_customurl($app);

        // // メニュー登録
        // $app['config'] = $app->share($app->extend('config', function ($config) {
        //     $addNavi['id'] = 'admin_SbmBlocks';
        //     $addNavi['name'] = 'カスタムURL管理';
        //     $addNavi['url'] = 'admin_SbmBlocks';
        //     $nav = $config['nav'];
        //     foreach ($nav as $key => $val) {
        //         if ('content' == $val['id']) {
        //             $nav[$key]['child'][] = $addNavi;
        //         }
        //     }
        //     $config['nav'] = $nav;
        //     return $config;
        // }));
    }

    private function bind_customurl(BaseApplication $app){

        // $SbmBlocksRepo = $app['eccube.plugin.SbmBlocks.repository.SbmBlocks'];

        // $Indexes = $SbmBlocksRepo->findBy(array('index_flg'=>1,'del_flg'=>0));
        // if($Indexes){
        //     foreach($Indexes as $IndexPage){
        //         //dump($IndexPage);
        //         $rt = $app->match($IndexPage->getCustomurl(), '\Plugin\SbmBlocks\Controller\Front\SbmBlocksController::index');

        //         $rt->value('indextype',$IndexPage->getPagecategorykey());
        //         if($IndexPage->getPageLayout()){
        //             $rt->bind($IndexPage->getPageLayout()->getUrl());

        //         }else{
        //             $rt->bind('SbmBlocks_list_'.$IndexPage->getPagecategorykey());

        //         }


        //     }

        // }

        // $CustomUrls = $SbmBlocksRepo->findBy(array('index_flg'=>0,'del_flg'=>0));
        // if($CustomUrls){
        //     foreach($CustomUrls as $CustomUrl){
        //         //dump($CustomUrl);
        //         //dump($CustomUrl->getPageLayout()->getUrl());
        //         if($CustomUrl->getPageLayout()){
        //             $app->match($CustomUrl->getCustomurl(), 'Plugin\SbmBlocks\Controller\Front\UserDataController::index')
        //             ->value('route', $CustomUrl->getPageLayout()->getUrl())
        //             ->bind($CustomUrl->getPageLayout()->getUrl());
        //             // dump($CustomUrl);

        //         }

        //     }
        // }

        /*

        // 一覧
        $app->match('/{listtype}', '\Plugin\SbmBlocks\Controller\Front\SbmBlocksController::index')
            ->bind('block_SbmBlocks_block');


        // user定義
        $app->match('/hogehoge', 'Plugin\ShoppingEx\Controller\UserDataController::index')->value('route', 'testpage1')->bind('testpage1');
        $app->match('/aaa/hogehoge2', 'Plugin\ShoppingEx\Controller\UserDataController::index')->value('route', 'testpage2')->bind('testpage2');

        */

    }

    public function boot(BaseApplication $app)
    {
    }
}
