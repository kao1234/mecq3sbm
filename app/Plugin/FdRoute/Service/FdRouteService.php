<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\FdRoute\Service;

use Eccube\Application;
use Eccube\Common\Constant;
use Symfony\Component\HttpFoundation\Session\Session;

class FdRouteService
{

    private $currsessionkey = 'smartphonestore.eccube.fdroute.current.fdroute.key_';
    /** @var \Eccube\Application */
    public $app;

    /** @var \Eccube\Entity\BaseInfo */
    public $BaseInfo;

    /**
     * コンストラクタ
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->BaseInfo = $app['eccube.repository.base_info']->get();
    }

    /**
     * おすすめ商品情報を新規登録する
     * @param $data
     * @return bool
     */
    public function createFdRoute($data) {
        // おすすめ商品詳細情報を生成する
        $FdRoute = $this->newFdRoute($data);

        $em = $this->app['orm.em'];

        // おすすめ商品情報を登録する
        $em->persist($FdRoute);

        $em->flush();

        return true;
    }

    /**
     * おすすめ商品情報を更新する
     * @param $data
     * @return bool
     */
    public function updateFdRoute($data) {
        $dateTime = new \DateTime();
        $em = $this->app['orm.em'];

        // おすすめ商品情報を取得する
        $FdRoute =$this->app['eccube.plugin.fdroute.repository.fdroute_product']->find($data['id']);
        if(is_null($FdRoute)) {
            false;
        }

        // おすすめ商品情報を書き換える
        $FdRoute->setConditions($data['conditions']);
        $FdRoute->setRouteString($data['route_string']);
        $FdRoute->setRouteStringPos($data['route_string_pos']);
        $FdRoute->setFdString($data['fd_string']);
        $FdRoute->setUpdateDate($dateTime);

        // おすすめ商品情報を更新する
        $em->persist($FdRoute);

        $em->flush();

        return true;
    }

    /**
     * おすすめ商品情報を削除する
     * @param $fdrouteId
     * @return bool
     */
    public function deleteFdRoute($fdrouteId) {
        $currentDateTime = new \DateTime();
        $em = $this->app['orm.em'];

        // おすすめ商品情報を取得する
        $FdRoute =$this->app['eccube.plugin.fdroute.repository.fdroute_product']->find($fdrouteId);
        if(is_null($FdRoute)) {
            false;
        }
        // おすすめ商品情報を書き換える
        $FdRoute->setDelFlg(Constant::ENABLED);
        $FdRoute->setUpdateDate($currentDateTime);

        // おすすめ商品情報を登録する
        $em->persist($FdRoute);

        $em->flush();

        return true;
    }

    /**
     * おすすめ商品情報の順位を上げる
     * @param $fdrouteId
     * @return bool
     */
    public function rankUp($fdrouteId) {
        $currentDateTime = new \DateTime();
        $em = $this->app['orm.em'];

        // おすすめ商品情報を取得する
        $FdRoute =$this->app['eccube.plugin.fdroute.repository.fdroute_product']->find($fdrouteId);
        if(is_null($FdRoute)) {
            false;
        }
        // 対象ランクの上に位置するおすすめ商品を取得する
        $TargetFdRoute =$this->app['eccube.plugin.fdroute.repository.fdroute_product']
                                ->findByRankUp($FdRoute->getRank());
        if(is_null($TargetFdRoute)) {
            false;
        }
        
        // ランクを入れ替える
        $rank = $TargetFdRoute->getRank();
        $TargetFdRoute->setRank($FdRoute->getRank());
        $FdRoute->setRank($rank);
        
        // 更新日設定
        $FdRoute->setUpdateDate($currentDateTime);
        $TargetFdRoute->setUpdateDate($currentDateTime);
        
        // 更新
        $em->persist($FdRoute);
        $em->persist($TargetFdRoute);

        $em->flush();
        return true;
    }

    /**
     * おすすめ商品情報の順位を下げる
     * @param $fdrouteId
     * @return bool
     */
    public function rankDown($fdrouteId) {
        $currentDateTime = new \DateTime();
        $em = $this->app['orm.em'];

        // おすすめ商品情報を取得する
        $FdRoute =$this->app['eccube.plugin.fdroute.repository.fdroute_product']->find($fdrouteId);
        if(is_null($FdRoute)) {
            false;
        }
        // 対象ランクの上に位置するおすすめ商品を取得する
        $TargetFdRoute =$this->app['eccube.plugin.fdroute.repository.fdroute_product']
                                ->findByRankDown($FdRoute->getRank());
        if(is_null($TargetFdRoute)) {
            false;
        }
        
        // ランクを入れ替える
        $rank = $TargetFdRoute->getRank();
        $TargetFdRoute->setRank($FdRoute->getRank());
        $FdRoute->setRank($rank);
        
        // 更新日設定
        $FdRoute->setUpdateDate($currentDateTime);
        $TargetFdRoute->setUpdateDate($currentDateTime);
        
        // 更新
        $em->persist($FdRoute);
        $em->persist($TargetFdRoute);

        $em->flush();

        return true;
    }

    /**
     * おすすめ商品情報を生成する
     * @param $data
     * @return \Plugin\FdRoute\Entity\FdRouteProduct
     */
    protected function newFdRoute($data) {
        $dateTime = new \DateTime();

        $rank = $this->app['eccube.plugin.fdroute.repository.fdroute_product']->getMaxRank();

        $FdRoute = new \Plugin\FdRoute\Entity\FdRouteProduct();

        $FdRoute->setConditions($data['conditions']);
        $FdRoute->setRouteString($data['route_string']);
        $FdRoute->setRouteStringPos($data['route_string_pos']);
        $FdRoute->setFdString($data['fd_string']);

        $FdRoute->setRank(($rank ? $rank : 0) + 1);
        $FdRoute->setDelFlg(Constant::DISABLED);
        $FdRoute->setCreateDate($dateTime);
        $FdRoute->setUpdateDate($dateTime);

        return $FdRoute;
    }

    private static $CurrFdRoute;
    //セッションに判定したFDルートを保存する
    /*
    判定はランク順に見て、ルートは最初に一致したもの、FDは最後に一致したもの
    */
    public function registFdRoute(){

        $app = $this->app;
        $req = $app['request'];
        $referer = $req->headers->get('referer');

        $query = $this->app['request']->query->all();
        $keys = array_keys($query);
        $includequery = false;
        $reservedparam = explode(',',$app['config']['fdroute_reservedparams']);

        //予約分のパラメータを除外するための前処理
        foreach($keys as $key){
            if(in_array($key
                ,$reservedparam)>0){
                //予約分のパラメータは無視

            }else{
                if(!empty($query[$key])){
                    $includequery = true;
                }
            }
        }
        //パラメータが付与されている場合、上書きするのでセッションに格納してある情報を削除
        if($includequery){
            $this->app['request']->getSession()->remove($this->currsessionkey);

        }
        $identitykeys = $this->app['request']->getSession()->get($this->currsessionkey);

        //すでにルート情報が記録されていれば、セッションに格納してあるものを返す
        if(count($identitykeys)>0){

            $param_arr = $this->app['request']->query->all();
            self::$CurrFdRoute = $identitykeys;

        }else{
        //判定処理
        //GET以外は判定しない

            if($this->app['request']->getMethod()=='GET'){

                $device = $app->isFrontRequest() & $app->isSmartPhone()?'SP':'PC';
                $device_condition="device=".$device;

                // FDルート管理一覧取得（rankの昇順）
                $list = $this->app['eccube.plugin.fdroute.repository.fdroute_product']->findList();

                $sesparam = $this->app['request']->query->all();
                
                $identitykeys['param'] = $sesparam;

                $param_arr = !empty($sesparam) ? $sesparam : array();

                $route_col = array(1=>'',2=>'',3=>'');
                $fd_num = '';
                foreach ($list as $key => $val) {
                    $conditionarr = explode(',',$val['conditions']);

                    $tmp_route_col= array(1=>'',2=>'',3=>'');
                    $tmp_fd_num=null;
                    //先頭にデバイスの指定を記述
                    if($conditionarr[0]==$device_condition){

                        foreach($conditionarr as $conditionstr){
                            $conditions = explode('=',$conditionstr);

                            if($conditions[0]=="device"){
                                //
                            }else{
                                if($conditions[0]=="REFERER"){
                                    //特別ルール：リファラーによる判定
                                    if(strpos($referer,$conditions[1])){

                                        $route_col[intval($val['route_string_pos'])] = 
                                            !empty($route_col[intval($val['route_string_pos'])])
                                                ? $route_col[intval($val['route_string_pos'])]
                                                : $val['route_string'].'_';
                                        $fd_num = $val['fd_string'];

                                    }

                                }else{
                                    if((array_key_exists($conditions[0],$param_arr) 
                                        && array_search($conditions[1],$param_arr)
                                        ) || $conditions[0] == '*'
                                    ){
                                        if($conditions[0] == '*'){
                                            $fd_num = 
                                                empty($fd_num)
                                                    ? $val['fd_string']
                                                    : $fd_num;
                                        }else{
                                            $route_col[intval($val['route_string_pos'])] = 
                                                !empty($route_col[intval($val['route_string_pos'])])
                                                    ? $route_col[intval($val['route_string_pos'])]
                                                    : $val['route_string'].'_';
                                            $fd_num = $val['fd_string'];
                                        }
                                    }


                                }

                            }

                        }

                    }

                }
                $identitykeys['fd_num'] = $fd_num;



                $route = 'WEB_'.$device.'_'.$route_col[1].$route_col[2].$route_col[3];
                $identitykeys['route_name'] = $route;

                self::$CurrFdRoute = $identitykeys;


                $this->app['request']->getSession()->set($this->currsessionkey,$identitykeys);  


            }


        }
//dump(self::$CurrFdRoute);
        return self::$CurrFdRoute;
    }
    //セッションに保存してあるFDルートを取得する
    public function getStoredFdRoute($session =null){

        if(empty(self::$CurrFdRoute)){
            if($session){
                self::$CurrFdRoute = $session->get($this->currsessionkey);

            }else{

                self::$CurrFdRoute = $this->app['request']->getSession()->get($this->currsessionkey);
            }
        }
        return self::$CurrFdRoute;
    }
    public function getStoredFdRouteNote(){

        //AB判定の出力

        return null;
    }
    public function isMobareco(){
        $result = false;

        $curr = $this->getSessionRouteInfo();

        if(isset($curr['route_name'])&&strpos($curr['route_name'],"モバレコ")){
            $result = true;
        }

        return $result;



    }

    public function isA8(){
        $curr = $this->getSessionRouteInfo();

        if($curr['param']['ac_source']=='a8'){
            return true;
        }
        return false;

    }

    protected function getSessionRouteInfo(){
        $app = $this->app;
        $result = false;
        $curr=array();
        if($app['request']->getSession()){
            $curr = $this->getStoredFdRoute();

        }else{
            // dump($app['session']);
            //kernel.app.beforeのタイミングでSessionが復元されていないので
            //無理やり復元
            $ss = new Session();
            if($app['request']->cookies->get('eccube')){
                $ss->setId($app['request']->cookies->get('eccube'));

            }
            if(!$ss->isStarted()){
                $ss->start();
            }
            $app['request']->setSession($ss);
            $this->registFdRoute();


            $curr = $this->getStoredFdRoute($ss);
            //$app['request']->setSession(new Session());

        }
        return $curr;


    }

}
