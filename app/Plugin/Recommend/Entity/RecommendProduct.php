<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
namespace Plugin\Recommend\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Eccube\Util\EntityUtil;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * RecommendProduct
 */
class RecommendProduct extends \Eccube\Entity\AbstractEntity
{

    /**
     *
     * @var integer
     */
    private $id;

    /**
     *
     * @var string
     */
    private $comment;

    /**
     *
     * @var integer
     */
    private $rank;

    /**
     *
     * @var integer
     */
    private $status;

    /**
     *
     * @var integer
     */
    private $del_flg;

    /**
     *
     * @var \DateTime
     */
    private $create_date;

    /**
     *
     * @var \DateTime
     */
    private $update_date;

    /**
     * @var \Eccube\Entity\Product
     */
    private $Product;

    private $RecommendProductImage;

    private $images;
    private $dlfilenames;

    private $add_images;
    private $add_dlfilenames;
    private $delete_images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->RecommendProductImage = new ArrayCollection();

    }

    /**
     * Get recommend product id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recommend product id
     *
     * @param integer $recommend_product_id
     * @return Module
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get commend
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param
     *            string
     * @return Module
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set rank
     *
     * @param
     *            integer
     * @return Module
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Set del_flg
     *
     * @param integer $delFlg
     * @return Order
     */
    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }

    /**
     * Get del_flg
     *
     * @return integer
     */
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    /**
     * Set create_date
     *
     * @param \DateTime $createDate
     * @return Module
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get create_date
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set update_date
     *
     * @param \DateTime $updateDate
     * @return Module
     */
    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }

    /**
     * Get update_date
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * Set Product
     *
     * @param \Eccube\Entity\Product $product
     * @return Prodcut
     */
    public function setProduct(\Eccube\Entity\Product $product)
    {
        $this->Product = $product;

        return $this;
    }

    /**
     * Get Product
     *
     * @return \Eccube\Entity\Product 
     */
    public function getProduct()
    {
        if (EntityUtil::isEmpty($this->Product)) {
            return null;
        }
        return $this->Product;
    }



    /**
     * Add ProductImage
     *
     * @param \Eccube\Entity\ProductImage $productImage
     * @return Product
     */
    public function addRecommendProductImage(\Plugin\Recommend\Entity\RecommendProductImage $image)
    {
        $this->RecommendProductImage[] = $image;

        return $this;
    }

    /**
     * Remove ProductImage
     *
     * @param \Eccube\Entity\ProductImage $productImage
     */
    public function removeRecommendProductImage(\Plugin\Recommend\Entity\RecommendProductImage $image)
    {
        $this->RecommendProductImage->removeElement($image);
    }


    public function getMainFileName()
    {
        if (count($this->RecommendProductImage) > 0) {
            return $this->RecommendProductImage[0];
        } else {
            return null;
        }
    }


    /**
     * Get Product
     *
     * @return \Plugin\ProductClassEx\Entity\ProductClassExImage
     */
    public function getRecommendProductImage()
    {
        return $this->RecommendProductImage;
    }

    /**
     * Set images
     *
     * @param  string       $images
     * @return ProductClassEx
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get stock
     *
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }


    public function setDlFilenames($images)
    {
        $this->dlfilenames = $images;

        return $this;
    }

    public function getDlFilenames()
    {
        return $this->dlfilenames;
    }


    /**
     * Set images
     *
     * @param  string       $images
     * @return ProductClassEx
     */
    public function setAddImages($images)
    {
        $this->add_images = $images;

        return $this;
    }

    /**
     * Get stock
     *
     * @return string
     */
    public function getAddImages()
    {
        return $this->add_images;
    }

    public function setAddDlFilenames($images)
    {
        $this->add_dlfilenames = $images;

        return $this;
    }

    public function getAddDlFilenames()
    {
        return $this->add_dlfilenames;
    }



    /**
     * Set images
     *
     * @param  string       $images
     * @return ProductClassEx
     */
    public function setDeleteImages($images)
    {
        $this->delete_images = $images;

        return $this;
    }

    /**
     * Get stock
     *
     * @return string
     */
    public function getDeleteImages()
    {
        return $this->delete_images;
    }


}
