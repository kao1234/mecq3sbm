<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\Recommend\Controller;

use Eccube\Application;
use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;

class RecommendController extends AbstractController
{

    private $main_title;

    private $sub_title;

    public function __construct()
    {
    }

    /**
     * おすすめ商品一覧
     * @param Application $app
     * @param Request     $request
     * @param unknown     $id
     * @throws NotFoundHttpException
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request)
    {
        $pagination = null;

        $pagination = $app['eccube.plugin.recommend.repository.recommend_product']->findList();

        return $app->render('Recommend/Resource/template/admin/index.twig', array(
            'pagination' => $pagination,
            'totalItemCount' => count($pagination)
        ));
    }

    /**
     * おすすめ商品の新規作成
     * @param Application $app
     * @param Request     $request
     * @param unknown     $id
     * @throws NotFoundHttpException
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Application $app, Request $request, $id)
    {

        $builder = $app['form.factory']->createBuilder('admin_recommend');
        $form = $builder->getForm();

        $service = $app['eccube.plugin.recommend.service.recommend'];

        $Product = null;

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            $data = $form->getData();
            if ($form->isValid()) {
                $Recommend = $service->createRecommend($data);

                if (!$Recommend) {
                    $app->addError('admin.recommend.notfound', 'admin');

                } else {
                    // 画像の登録
                    $add_images = $form->get('add_images')->getData();
                    $add_dlfilenames = $form->get('add_dlfilenames')->getData();
                    foreach ($add_images as $k=>$add_image) {
                        $add_dlfilename=$add_dlfilenames[$k];

                        $RecommendProductImage = new \Plugin\Recommend\Entity\RecommendProductImage();
                        $RecommendProductImage
                            ->setFileName($add_image)
                            ->setDlFileName($add_dlfilename)

                            ->setRecommendProduct($Recommend)
                            ->setRank(1);
                        $Recommend->addRecommendProductImage($RecommendProductImage);
                        $app['orm.em']->persist($RecommendProductImage);
                        $app['orm.em']->flush();

                        // 移動
                        $file = new File($app['config']['image_temp_realdir'] . '/' . $add_image);
                        $file->move($app['config']['image_save_realdir']);
                    }

                    $ranks = $request->get('rank_images');
                    if ($ranks) {
                        foreach ($ranks as $rank) {
                            list($filename, $rank_val) = explode('//', $rank);
                            $RecommendProductImage = $app['eccube.plugin.recommend.repository.recommendproductimage']
                                ->findOneBy(array(
                                    'filename' => $filename,
                                    'RecommendProduct' => $Recommend,
                                ));
                            $RecommendProductImage->setRank($rank_val);
                            $app['orm.em']->persist($RecommendProductImage);
                        }
                    }
                    $app['orm.em']->flush();




                    $app->addSuccess('admin.plugin.recommend.regist.success', 'admin');
                }

                return $app->redirect($app->url('admin_recommend_list'));
            }

            if (!is_null($data['Product'])) {
                $Product = $data['Product'];
            }
        }

        return $this->renderRegistView(
            $app,
            array(
                'form' => $form->createView(),
                'Product' => $Product
            )
        );
    }

    /**
     * 編集
     * @param Application $app
     * @param Request     $request
     * @param unknown     $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Application $app, Request $request, $id)
    {

        if (is_null($id) || strlen($id) == 0) {
            $app->addError("admin.recommend.recommend_id.notexists", "admin");
            return $app->redirect($app->url('admin_recommend_list'));
        }

        $service = $app['eccube.plugin.recommend.service.recommend'];

        // IDからおすすめ商品情報を取得する
        $Recommend = $app['eccube.plugin.recommend.repository.recommend_product']->findById($id);

        if (is_null($Recommend)) {
            $app->addError('admin.recommend.notfound', 'admin');
            return $app->redirect($app->url('admin_recommend_list'));
        }

        $Recommend = $Recommend[0];

        // formの作成
        $form = $app['form.factory']
            ->createBuilder('admin_recommend', $Recommend)
            ->getForm();

        $images = array();
        $dlfilenames = array();
        $RecommendProductImages = $Recommend->getRecommendProductImage();
        foreach ($RecommendProductImages as $RecommendProductImage) {
            $images[] = $RecommendProductImage->getFileName();
            $dlfilenames[] = $RecommendProductImage->getDlFileName();

        }
        $Recommend->setImages($images);
        $form['images']->setData($images);

        $Recommend->setDlFileNames($dlfilenames);
        $form['dlfilenames']->setData($dlfilenames);


        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $status = $service->updateRecommend($form->getData());

                if (!$status) {
                    $app->addError('admin.recommend.notfound', 'admin');
                } else {
                    $app->addSuccess('admin.plugin.recommend.update.success', 'admin');
                }

                // 画像の登録
                $add_images = $form->get('add_images')->getData();
                $add_dlfilenames = $form->get('add_dlfilenames')->getData();
                foreach ($add_images as $k=>$add_image) {
                    $add_dlfilename=$add_dlfilenames[$k];

                    $RecommendProductImage = new \Plugin\Recommend\Entity\RecommendProductImage();
                    $RecommendProductImage
                        ->setFileName($add_image)
                        ->setDlFileName($add_dlfilename)

                        ->setRecommendProduct($Recommend)
                        ->setRank(1);
                    $Recommend->addRecommendProductImage($RecommendProductImage);
                    $app['orm.em']->persist($RecommendProductImage);
                    $app['orm.em']->flush();

                    // 移動
                    $file = new File($app['config']['image_temp_realdir'] . '/' . $add_image);
                    $file->move($app['config']['image_save_realdir']);
                }



                // 画像の削除
                $delete_images = $form->get('delete_images')->getData();
                foreach ($delete_images as $delete_image) {
                    $RecommendProductImage = $app['eccube.plugin.recommend.repository.recommendproductimage']
                        ->findOneBy(array('filename' => $delete_image));

                    // 追加してすぐに削除した画像は、Entityに追加されない
                    if ($RecommendProductImage instanceof \Plugin\Recommend\Entity\RecommendProductImage) {
                        $Recommend->removeRecommendProductImage($RecommendProductImage);
                        $app['orm.em']->remove($RecommendProductImage);

                    }
                    $app['orm.em']->persist($Recommend);

                    // 削除
                    $fs = new Filesystem();
                    $fs->remove($app['config']['image_save_realdir'] . '/' . $delete_image);
                }
                $app['orm.em']->persist($Recommend);
                $app['orm.em']->flush();


                $ranks = $request->get('rank_images');
                if ($ranks) {
                    foreach ($ranks as $rank) {
                        list($filename, $rank_val) = explode('//', $rank);
                        $RecommendProductImage = $app['eccube.plugin.recommend.repository.recommendproductimage']
                            ->findOneBy(array(
                                'filename' => $filename,
                                'RecommendProduct' => $Recommend,
                            ));
                        $RecommendProductImage->setRank($rank_val);
                        $app['orm.em']->persist($RecommendProductImage);
                    }
                }
                $app['orm.em']->flush();





                return $app->redirect($app->url('admin_recommend_list'));
            }
        }

        return $this->renderRegistView(
            $app,
            array(
                'form' => $form->createView(),
                'Product' => $Recommend->getProduct()
            )
        );
    }

    /**
     * おすすめ商品の削除
     * @param Application $app
     * @param Request     $request
     * @param unknown     $id
     * @throws NotFoundHttpException
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Application $app, Request $request, $id)
    {

        $this->isTokenValid($app);

        if (!'POST' === $request->getMethod()) {
            throw new HttpException();
        }
        if (is_null($id) || strlen($id) == 0) {
            $app->addError("admin.recommend.recommend_id.notexists", "admin");
            return $app->redirect($app->url('admin_recommend_list'));
        }


        $service = $app['eccube.plugin.recommend.service.recommend'];

        // おすすめ商品情報を削除する
        if ($service->deleteRecommend($id)) {
            $app->addSuccess('admin.plugin.recommend.delete.success', 'admin');
        } else {
            $app->addError('admin.recommend.notfound', 'admin');
        }

        return $app->redirect($app->url('admin_recommend_list'));

    }

    /**
     * 上へ
     * @param Application $app
     * @param Request     $request
     * @param unknown     $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rankUp(Application $app, Request $request, $id)
    {

        $this->isTokenValid($app);

        if (is_null($id) || strlen($id) == 0) {
            $app->addError("admin.recommend.recommend_id.notexists", "admin");
            return $app->redirect($app->url('admin_recommend_list'));
        }

        $service = $app['eccube.plugin.recommend.service.recommend'];

        // IDからおすすめ商品情報を取得する
        $Recommend = $app['eccube.plugin.recommend.repository.recommend_product']->find($id);
        if (is_null($Recommend)) {
            $app->addError('admin.recommend.notfound', 'admin');
            return $app->redirect($app->url('admin_recommend_list'));
        }

        // ランクアップ
        $service->rankUp($id);

        $app->addSuccess('admin.plugin.recommend.complete.up', 'admin');

        return $app->redirect($app->url('admin_recommend_list'));
    }

    /**
     * 下へ
     * @param Application $app
     * @param Request     $request
     * @param unknown     $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rankDown(Application $app, Request $request, $id)
    {

        $this->isTokenValid($app);

        if (is_null($id) || strlen($id) == 0) {
            $app->addError("admin.recommend.recommend_id.notexists", "admin");
            return $app->redirect($app->url('admin_recommend_list'));
        }

        $service = $app['eccube.plugin.recommend.service.recommend'];

        // IDからおすすめ商品情報を取得する
        $Recommend = $app['eccube.plugin.recommend.repository.recommend_product']->find($id);
        if (is_null($Recommend)) {
            $app->addError('admin.recommend.notfound', 'admin');
            return $app->redirect($app->url('admin_recommend_list'));
        }

        // ランクアップ
        $service->rankDown($id);

        $app->addSuccess('admin.plugin.recommend.complete.down', 'admin');

        return $app->redirect($app->url('admin_recommend_list'));
    }

    /**
     * 編集画面用のrender
     * @param unknown $app
     * @param unknown $parameters
     */
    protected function renderRegistView($app, $parameters = array())
    {
        // 商品検索フォーム
        $searchProductModalForm = $app['form.factory']->createBuilder('admin_search_product')->getForm();
        $viewParameters = array(
            'searchProductModalForm' => $searchProductModalForm->createView(),
        );
        $viewParameters += $parameters;
        return $app->render('Recommend/Resource/template/admin/regist.twig', $viewParameters);
    }


    public function addImage(Application $app, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException('リクエストが不正です');
        }

        $images = $request->files->get('admin_recommend');

        $files = array();
        if (count($images) > 0) {
            foreach ($images as $img) {
                foreach ($img as $image) {
                    //ファイルフォーマット検証
                    $mimeType = $image->getMimeType();
                    if (strpos($mimeType, 'image')>0) {

                        //throw new UnsupportedMediaTypeHttpException('ファイル形式が不正です');
                    }elseif (strpos($mimeType, 'pdf')>9) {

                    }else{
                        //throw new UnsupportedMediaTypeHttpException('ファイル形式が不正です');                        
                    }
                    $extension = $image->getClientOriginalExtension();
                    $filename = array(
                        'name'=>date('mdHis') . uniqid('_') . '.' . $extension,
                        'dlname'=>$image->getClientOriginalName(),

                        );
                    $image->move($app['config']['image_temp_realdir'], $filename['name']);
                    $files[] = $filename;
                }
            }
        }


        return $app->json(array('files' => $files), 200);
    }

}
