<?php
/*
 * This file is part of the ProductSortColumn
 *
 * Copyright(c) 2017 izayoi256 All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\ProductSortColumn\Controller\Block;

use Doctrine\ORM\QueryBuilder;
use Eccube\Application;
use Eccube\Controller\AbstractController;
use Eccube\Event\EventArgs;
use Plugin\ProductSortColumn\Entity\Info;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractController
{
    /**
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request,$block=null)
    {
        /** @var Info $Info */
        $Info = $app['eccube.plugin.product_sort_column.repository.info']->get();
        $repository = $app['eccube.plugin.product_sort_column.repository.master.product_list_order_by'];

        $getProducts = function ($searchData) use ($app) {
            /** @var QueryBuilder $qb */
            $qb = $app['eccube.repository.product']->getQueryBuilderBySearchData($searchData);
            $event = new EventArgs(compact('qb', 'searchData'));
            $app['eccube.plugin.product_sort_column.event.event']->onProductSearch($event);
            return $app['paginator']()->paginate($qb, 1, $app['product_sort_column_config']['product_sort_list_limit']);
        };


        if($app['eccube.recommend.service.recommend']->checkInstallPlugin('Recommend')){
            $ttt1 = $getProducts(array('orderby' => $repository->find(1)));
            $product_param1 = $app['eccube.recommend.service.recommend']->getProductArrayParam($ttt1->getItems());
            $ttt2 = $getProducts(array('orderby' => $repository->find(2)));
            $product_param2 = $app['eccube.recommend.service.recommend']->getProductArrayParam($ttt2->getItems());
            $ttt3 = $getProducts(array('orderby' => $Info->getSort01()));
            $product_param3 = $app['eccube.recommend.service.recommend']->getProductArrayParam($ttt3->getItems());

        }
// dump($product_param3['__EX_PRODUCT_LIST'][132][86]);
// dump($product_param3['__EX_PRODUCT_LIST'][132][86]['value']);
// dump($product_param3['__EX_PRODUCT_LIST'][132][86]['valuetext']);
        $parameters = array(
            'LowerPriceProducts'    => $ttt1,
            'NewerProducts'         => $ttt2,
            'Sort01Products'        => $ttt3,
            '__EX_PRODUCT_LIST1'         => $product_param1['__EX_PRODUCT_LIST'],
            '__EX_PRODUCT_LIST_MAKER1'   => $product_param1['__EX_PRODUCT_LIST_MAKER'],
            '__EX_PRODUCT_LIST2'         => $product_param2['__EX_PRODUCT_LIST'],
            '__EX_PRODUCT_LIST_MAKER2'   => $product_param2['__EX_PRODUCT_LIST_MAKER'],
            '__EX_PRODUCT_LIST3'         => $product_param3['__EX_PRODUCT_LIST'],
            '__EX_PRODUCT_LIST_MAKER3'   => $product_param3['__EX_PRODUCT_LIST_MAKER'],
    );
        return $app->render('Block/plugin_product_sort_column_product_'.$block.'.twig', $parameters);
    }



}
