<?php
/*
 * This file is part of the ProductSortColumn
 *
 * Copyright(c) 2017 izayoi256 All Rights Reserved.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Plugin\ProductSortColumn\ControllerProvider;

use Silex\Application;
use Silex\ControllerProviderInterface;

class FrontControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $c = $app['controllers_factory'];

        // 強制SSL
        if ($app['config']['force_ssl'] == \Eccube\Common\Constant::ENABLED) {
            $c->requireHttps();
        }

        $c->match('/block/plugin_product_sort_column_list', '\Plugin\ProductSortColumn\Controller\Block\ProductController::index')->value('block','list')->bind('block_plugin_product_sort_column_product_list');

        $c->match('/block/plugin_product_sort_column_list2', '\Plugin\ProductSortColumn\Controller\Block\ProductController::index')->value('block','list2')->bind('block_plugin_product_sort_column_product_list2');

        $c->match('/block/plugin_product_sort_column_list3', '\Plugin\ProductSortColumn\Controller\Block\ProductController::index')->value('block','list3')->bind('block_plugin_product_sort_column_product_list3');


        return $c;
    }
}
