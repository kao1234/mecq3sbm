<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161120000000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->createPluginTable($schema);
    }

    public function down(Schema $schema)
    {
        $schema->dropTable('plg_shiro8_faq_group');
        $schema->dropTable('plg_shiro8_faq');
    }

    protected function createPluginTable(Schema $schema)
    {
        $table = $schema->createTable("plg_shiro8_faq_group");
        $table->addColumn('faq_group_id', 'integer', array(
            'autoincrement' => true,
            'notnull' => true,
        ));
        $table->addColumn('creator_id', 'integer', array('notnull' => false));
        $table->addColumn('faq_group_name', 'text', array('notnull' => true));
        $table->addColumn('rank', 'integer', array(
            'notnull' => true,
            'unsigned' => false,
            'default' => 1,
        ));
        $table->addColumn('del_flg', 'smallint', array(
            'notnull' => true,
            'unsigned' => false,
            'default' => 0,
        ));
        $table->addColumn('create_date', 'datetime', array(
            'notnull' => true,
            'unsigned' => false,
        ));
        $table->addColumn('update_date', 'datetime', array(
            'notnull' => true,
            'unsigned' => false,
        ));
        $table->setPrimaryKey(array('faq_group_id'));
        
        $table = $schema->createTable("plg_shiro8_faq");
        $table->addColumn('faq_id', 'integer', array(
            'autoincrement' => true,
            'notnull' => true,
        ));
        $table->addColumn('faq_group_id', 'integer', array('notnull' => true));
        $table->addColumn('creator_id', 'integer', array('notnull' => false));
        $table->addColumn('question', 'text', array('notnull' => true));
        $table->addColumn('answer', 'text', array('notnull' => true));
        $table->addColumn('rank', 'integer', array(
            'notnull' => true,
            'unsigned' => false,
            'default' => 1,
        ));
        $table->addColumn('del_flg', 'smallint', array(
            'notnull' => true,
            'unsigned' => false,
            'default' => 0,
        ));
        $table->addColumn('create_date', 'datetime', array(
            'notnull' => true,
            'unsigned' => false,
        ));
        $table->addColumn('update_date', 'datetime', array(
            'notnull' => true,
            'unsigned' => false,
        ));
        $table->setPrimaryKey(array('faq_id'));
    }
}