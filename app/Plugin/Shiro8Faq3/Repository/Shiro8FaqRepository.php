<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Plugin\Shiro8Faq3\Repository;

use Doctrine\ORM\EntityRepository;

class Shiro8FaqRepository extends EntityRepository
{
    /**
     * FAQ一覧を取得する.
     *
     * @return array FAQの配列
     */
    public function getList(\Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup = null)
    {
        $qb = $this->createQueryBuilder('f')
            ->orderBy('f.rank', 'DESC');
        if ($FaqGroup) {
            $qb->where('f.Shiro8FaqGroup = :FaqGroup')->setParameter('FaqGroup', $FaqGroup);
        }
        $Faqs = $qb->getQuery()
            ->getResult();

        return $Faqs;
    }
    
    /**
     * FAQの順位を1上げる.
     *
     * @param  \Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq
     * @return boolean 成功した場合 true
     */
    public function up(\Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $rank = $Faq->getRank();

            //
            $Faq2 = $this->findOneBy(array('rank' => $rank + 1));
            if (!$Faq2) {
                throw new \Exception();
            }
            $Faq2->setRank($rank);
            $em->persist($Faq);

            // Faq更新
            $Faq->setRank($rank + 1);

            $em->persist($Faq);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * FAQの順位を1下げる.
     *
     * @param \Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq
     * @return boolean 成功した場合 true
     */
    public function down(\Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $rank = $Faq->getRank();

            //
            $Faq2 = $this->findOneBy(array('rank' => $rank - 1));
            if (!$Faq2) {
                throw new \Exception();
            }
            $Faq2->setRank($rank);
            $em->persist($Faq);

            // Faq更新
            $Faq->setRank($rank - 1);

            $em->persist($Faq);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * FAQを保存する.
     *
     * @param \Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq
     * @return boolean 成功した場合 true
     */
    public function save(\Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            if (!$Faq->getId()) {
                $FaqGroup = $Faq->getShiro8FaqGroup();
                $rank = $this->createQueryBuilder('f')
                    ->select('MAX(f.rank)')
                    ->where('f.Shiro8FaqGroup = :FaqGroup')->setParameter('FaqGroup', $FaqGroup)
                    ->getQuery()
                    ->getSingleScalarResult();
                if (!$rank) {
                    $rank = 0;
                }
                $Faq->setRank($rank + 1);
                $Faq->setDelFlg(0);
            }

            $em->persist($Faq);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * FAQを削除する.
     *
     * @param \Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq
     * @return boolean 成功した場合 true
     */
    public function delete(\Plugin\Shiro8Faq3\Entity\Shiro8Faq $Faq)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {

            $rank = $Faq->getRank();
            $FaqGroup = $Faq->getShiro8FaqGroup();
            
            $em->createQueryBuilder()
                ->update('Plugin\Shiro8Faq3\Entity\Shiro8Faq', 'f')
                ->set('f.rank', 'f.rank - 1')
                ->where('f.rank > :rank AND f.Shiro8FaqGroup = :FaqGroup')
                ->setParameter('rank', $rank)
                ->setParameter('FaqGroup', $FaqGroup)
                ->getQuery()
                ->execute();

            $Faq->setDelFlg(1);
            $em->persist($Faq);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }
}