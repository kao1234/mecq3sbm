<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Plugin\Shiro8Faq3\Repository;

use Doctrine\ORM\EntityRepository;

class Shiro8FaqGroupRepository extends EntityRepository
{

    /**
     * FAQグループ一覧を取得する.
     *
     * @return array FAQグループの配列
     */
    public function getList()
    {
        $qb = $this->createQueryBuilder('fg')
            ->orderBy('fg.rank', 'DESC');
        $FaqGroups = $qb->getQuery()
            ->getResult();

        return $FaqGroups;
    }
    
    /**
     * FAQグループの順位を1上げる.
     *
     * @param  \Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup
     * @return boolean 成功した場合 true
     */
    public function up(\Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $rank = $FaqGroup->getRank();

            //
            $FaqGroup2 = $this->findOneBy(array('rank' => $rank + 1));
            if (!$FaqGroup2) {
                throw new \Exception();
            }
            $FaqGroup2->setRank($rank);
            $em->persist($FaqGroup);

            // FaqGroup更新
            $FaqGroup->setRank($rank + 1);

            $em->persist($FaqGroup);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * FAQグループの順位を1下げる.
     *
     * @param \Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup
     * @return boolean 成功した場合 true
     */
    public function down(\Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $rank = $FaqGroup->getRank();

            //
            $FaqGroup2 = $this->findOneBy(array('rank' => $rank - 1));
            if (!$FaqGroup2) {
                throw new \Exception();
            }
            $FaqGroup2->setRank($rank);
            $em->persist($FaqGroup);

            // FaqGroup更新
            $FaqGroup->setRank($rank - 1);

            $em->persist($FaqGroup);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * FAQグループを保存する.
     *
     * @param \Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup
     * @return boolean 成功した場合 true
     */
    public function save(\Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup)
    {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            if (!$FaqGroup->getId()) {
                $rank = $this->createQueryBuilder('fg')
                    ->select('MAX(fg.rank)')
                    ->getQuery()
                    ->getSingleScalarResult();
                if (!$rank) {
                    $rank = 0;
                }
                $FaqGroup->setRank($rank + 1);
                $FaqGroup->setDelFlg(0);
            }

            $em->persist($FaqGroup);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
        	print $e->getMessage();
        	exit;
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

    /**
     * FAQグループを削除する.
     *
     * @param \Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup
     * @return boolean 成功した場合 true
     */
    public function delete(\Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $FaqGroup)
    {
    
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            if ($FaqGroup->getShiro8Faqs()->count() > 0) {
                throw new \Exception();
            }

            $rank = $FaqGroup->getRank();
            $em->createQueryBuilder()
                ->update('Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup', 'fg')
                ->set('fg.rank', 'fg.rank - 1')
                ->where('fg.rank > :rank')->setParameter('rank', $rank)
                ->getQuery()
                ->execute();

            $FaqGroup->setDelFlg(1);
            $em->persist($FaqGroup);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollback();

            return false;
        }

        return true;
    }

}