<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\Shiro8Faq3\ServiceProvider;

use Silex\Application as BaseApplication;
use Silex\ServiceProviderInterface;
use Symfony\Component\Yaml\Yaml;

class Shiro8Faq3ServiceProvider implements ServiceProviderInterface
{

    public function register(BaseApplication $app)
    {
        // FAQ情報テーブルリポジトリ
        $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup');
        });
        
        // FAQグループ情報テーブルリポジトリ
        $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Plugin\Shiro8Faq3\Entity\Shiro8Faq');
        });

        // FAQグループの一覧
        $admin_dir = '/' . trim($app['config']['admin_route']);
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faqGroup',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqGroupController::index')
            ->value('id', null)->assert('id', '\d+|')
            ->bind('admin_shiro8_faq_group');
            
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faqGroup/{id}/edit',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqGroupController::index')
            ->assert('id', '\d+|')
            ->bind('admin_shiro8_faq_group_edit');
        
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faqGroup/rank/move',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqGroupController::moveRank')
            ->bind('admin_shiro8_faq_group_rank_move');
            
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faqGroup/{id}/delete',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqGroupController::delete')
            ->assert('id', '\d+|')
            ->bind('admin_shiro8_faq_group_delete');
            
        // FAQの一覧
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faq/{faq_group_id}',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqController::index')
            ->assert('faq_group_id', '\d+|')
            ->bind('admin_shiro8_faq');
            
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faq/{faq_group_id}/{id}/edit',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqController::index')
            ->assert('faq_group_id', '\d+|')
            ->assert('id', '\d+|')
            ->bind('admin_shiro8_faq_edit');
        
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faq/rank/move',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqController::moveRank')
            ->bind('admin_shiro8_faq_rank_move');
            
        $app->match($admin_dir . '/plugin/Shiro8Faq3/faq/{faq_group_id}/{id}/delete',
                    '\Plugin\Shiro8Faq3\Controller\Shiro8FaqController::delete')
            ->assert('faq_group_id', '\d+|')
            ->assert('id', '\d+|')
            ->bind('admin_shiro8_faq_delete');

        // メッセージ登録
        $app['translator'] = $app->share($app->extend('translator', function ($translator, \Silex\Application $app) {
            $translator->addLoader('yaml', new \Symfony\Component\Translation\Loader\YamlFileLoader());

            $file = __DIR__ . '/../Resource/locale/message.' . $app['locale'] . '.yml';
            if (file_exists($file)) {
                $translator->addResource('yaml', $file, $app['locale']);
            }

            return $translator;
        }));

        // ブロック
        $app->match('/block/plg_shiro8_faq_block', '\Plugin\Shiro8Faq3\Controller\Block\Shiro8FaqController::index')
            ->bind('block_plg_shiro8_faq_block');


        // 型登録
        $app['form.types'] = $app->share($app->extend('form.types', function ($types) use ($app) {
            $types[] = new \Plugin\Shiro8Faq3\Form\Type\FaqGroupType($app);
            return $types;
        }));
        
        $app['form.types'] = $app->share($app->extend('form.types', function ($types) use ($app) {
            $types[] = new \Plugin\Shiro8Faq3\Form\Type\FaqType($app);
            return $types;
        }));

        // メニュー登録
        $app['config'] = $app->share($app->extend('config', function ($config) {
            //グループ追加
            $addNavi['id'] = 'shiro8_faq_group';
            $addNavi['name'] = 'FAQ管理';
            $addNavi['url'] = 'admin_shiro8_faq_group';
            
            $nav = $config['nav'];
            foreach ($nav as $key => $val) {
                if ('content' == $val['id']) {
                    $nav[$key]['child'][] = $addNavi;
                }
            }
            $config['nav'] = $nav;
            return $config;
        }));
    }

    public function boot(BaseApplication $app)
    {
    }
}
