<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Plugin\Shiro8Faq3\Entity;


class Shiro8FaqGroup extends \Eccube\Entity\AbstractEntity
{
    private $id;
    private $faq_group_name;
    private $rank;
    private $del_flg;
    private $create_date;
    private $update_date;
    
    private $Shiro8Faqs;
    
    private $Creator;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->Shiro8Faqs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getFaqGroupName()
    {
        return $this->faq_group_name;
    }

    public function setFaqGroupName($faqGroupName)
    {
        $this->faq_group_name = $faqGroupName;

        return $this;
    }
    
    public function getRank()
    {
        return $this->rank;
    }

    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }
    
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }
    
    public function getCreateDate()
    {
        return $this->create_date;
    }

    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }
    
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }
    
    public function addShiro8Faq(\Plugin\Shiro8Faq3\Entity\Shiro8Faq $shiro8Faqs)
    {
        $this->Shiro8Faqs[] = $shiro8Faqs;

        return $this;
    }
    
    public function removeShiro8Faq(\Plugin\Shiro8Faq3\Entity\Shiro8Faq $shiro8Faqs)
    {
        $this->Shiro8Faqs->removeElement($shiro8Faqs);
    }
    
    public function getShiro8Faqs()
    {
        return $this->Shiro8Faqs;
    }
    
    public function setCreator(\Eccube\Entity\Member $creator)
    {
        $this->Creator = $creator;

        return $this;
    }
    
    public function getCreator()
    {
        return $this->Creator;
    }
}
