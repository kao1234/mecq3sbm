<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Plugin\Shiro8Faq3\Entity;

use Eccube\Util\EntityUtil;

class Shiro8Faq extends \Eccube\Entity\AbstractEntity
{
    private $id;
    private $question;
    private $answer;
    private $rank;
    private $del_flg;
    private $create_date;
    private $update_date;
    
    private $Shiro8FaqGroup;
    
    private $Creator;

	public function getId()
    {
        return $this->id;
    }
    
    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }
    
    public function getAnswer()
    {
        return $this->answer;
    }

    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }
    
    public function getRank()
    {
        return $this->rank;
    }

    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }
    
    public function getDelFlg()
    {
        return $this->del_flg;
    }

    public function setDelFlg($delFlg)
    {
        $this->del_flg = $delFlg;

        return $this;
    }
    
    public function getCreateDate()
    {
        return $this->create_date;
    }

    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }
    
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    public function setUpdateDate($updateDate)
    {
        $this->update_date = $updateDate;

        return $this;
    }
    
    public function setShiro8FaqGroup(\Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup $shiro8FaqGroup)
    {
        $this->Shiro8FaqGroup = $shiro8FaqGroup;

        return $this;
    }
    
    public function getShiro8FaqGroup()
    {
        if (EntityUtil::isEmpty($this->Shiro8FaqGroup)) {
            return null;
        }
        return $this->Shiro8FaqGroup;
    }
    
    public function setCreator(\Eccube\Entity\Member $creator)
    {
        $this->Creator = $creator;

        return $this;
    }
    
    public function getCreator()
    {
        return $this->Creator;
    }
}
