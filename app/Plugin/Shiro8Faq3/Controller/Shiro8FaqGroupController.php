<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Plugin\Shiro8Faq3\Controller;

use Eccube\Application;
use Eccube\Controller\AbstractController;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

class Shiro8FaqGroupController extends AbstractController
{

    public $form;

    public function __construct()
    {
    }

    public function index(Application $app, Request $request, $id = null)
    {
    
        $Help = $app['eccube.repository.help']->get();
        
        if ($id) {
            $TargetFaqGroup = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->find($id);
            if (!$TargetFaqGroup) {
                throw new NotFoundHttpException('FAQグループが存在しません');
            }
        } else {
            $TargetFaqGroup = new \Plugin\Shiro8Faq3\Entity\Shiro8FaqGroup();
        }
        
        $builder = $app['form.factory']
            ->createBuilder('admin_plg_shiro8_faq_group', $TargetFaqGroup);
        
        $form = $builder->getForm();

        if ($request->getMethod() === 'POST') {
        	$FaqGroups = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->getList();
        	if ($id || count($FaqGroups) < 6) {
	            $form->handleRequest($request);
	            if ($form->isValid()) {
	                log_info('FAQグループ登録開始', array($id));
	                $status = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->save($TargetFaqGroup);

	                if ($status) {
	                    log_info('FAQグループ登録完了', array($id));
	                    $app->addSuccess('admin.plugin.faqgroup.save.complete', 'admin');
	                    return $app->redirect($app->url('admin_shiro8_faq_group'));
	                } else {
	                    log_info('FAQグループ登録エラー', array($id));
	                    $app->addError('admin.plugin.faqgroup.save.error', 'admin');
	                }
	            }
            } else {
            	log_info('FAQグループ登録エラー', array($id));
	            $app->addError('admin.plugin.faqgroup.max.error', 'admin');
            }
        }

        $FaqGroups = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->getList();

        return $app->render('Shiro8Faq3/Resource/template/Admin/faq_group.twig', array(
        	'form' => $form->createView(),
        	'FaqGroups' => $FaqGroups,
            'TargetFaqGroup' => $TargetFaqGroup,
        ));

    }
    
    public function delete(Application $app, Request $request, $id)
    {
        $this->isTokenValid($app);

        $TargetFaqGroup = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->find($id);
        if (!$TargetFaqGroup) {
            $app->deleteMessage();
            return $app->redirect($app->url('admin_shiro8_faq_group'));
        }

        log_info('FAQグループ削除開始', array($id));

        $status = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->delete($TargetFaqGroup);

        if ($status === true) {
            log_info('FAQグループ削除完了', array($id));

            $app->addSuccess('admin.plugin.faqgroup.delete.complete', 'admin');
        } else {
            $app->addError('admin.plugin.faqgroup.delete.error', 'admin');
        }

        return $app->redirect($app->url('admin_shiro8_faq_group'));
    }

    public function moveRank(Application $app, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $ranks = $request->request->all();
            foreach ($ranks as $faqGroupsId => $rank) {
                $FaqGroup = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']
                    ->find($faqGroupsId);
                $FaqGroup->setRank($rank);
                $app['orm.em']->persist($FaqGroup);
            }
            $app['orm.em']->flush();
        }
        return true;
    }
}
