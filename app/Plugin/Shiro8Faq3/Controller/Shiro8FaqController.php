<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Plugin\Shiro8Faq3\Controller;

use Eccube\Application;
use Eccube\Controller\AbstractController;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

class Shiro8FaqController extends AbstractController
{

    public $form;

    public function __construct()
    {
    }

    public function index(Application $app, Request $request, $faq_group_id, $id = null)
    {
    
        $Help = $app['eccube.repository.help']->get();
        
        //親情報を取得
        $FaqGroup = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->find($faq_group_id);
        if (!$FaqGroup) {
            throw new NotFoundHttpException('FAQグループが存在しません');
        }
        
        if ($id) {
            $TargetFaq = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq']->find($id);
            if (!$TargetFaq) {
                throw new NotFoundHttpException('FAQが存在しません');
            }
        } else {
            $TargetFaq = new \Plugin\Shiro8Faq3\Entity\Shiro8Faq();
            $TargetFaq->setShiro8FaqGroup($FaqGroup);
        }
        
        $builder = $app['form.factory']
            ->createBuilder('admin_plg_shiro8_faq', $TargetFaq);
        
        $form = $builder->getForm();

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                log_info('FAQ登録開始', array($id));
                $status = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq']->save($TargetFaq);

                if ($status) {
                    log_info('FAQ登録完了', array($id));
                    $app->addSuccess('admin.plugin.faqgroup.save.complete', 'admin');
                    return $app->redirect($app->url('admin_shiro8_faq', array('faq_group_id' => $FaqGroup->getId())));
                } else {
                    log_info('FAQ登録エラー', array($id));
                    $app->addError('admin.plugin.faqgroup.save.error', 'admin');
                }
            }
        }

        $Faqs = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq']->getList($FaqGroup);

        return $app->render('Shiro8Faq3/Resource/template/Admin/faq.twig', array(
        	'form' => $form->createView(),
        	'FaqGroup' => $FaqGroup,
        	'Faqs' => $Faqs,
            'TargetFaq' => $TargetFaq,
        ));

    }
    
    public function delete(Application $app, Request $request, $faq_group_id, $id)
    {
        $this->isTokenValid($app);
        
        //親情報を取得
        $FaqGroup = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq_group']->find($faq_group_id);
        if (!$FaqGroup) {
            throw new NotFoundHttpException('FAQグループが存在しません');
        }

        $TargetFaq = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq']->find($id);
        if (!$TargetFaq || $TargetFaq->getShiro8FaqGroup() != $FaqGroup) {
            $app->deleteMessage();
            return $app->redirect($app->url('admin_shiro8_faq', array('faq_group_id' => $FaqGroup->getId())));
        }

        log_info('FAQ削除開始', array($id));

        $status = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq']->delete($TargetFaq);

        if ($status === true) {
            log_info('FAQ削除完了', array($id));

            $app->addSuccess('admin.plugin.faqgroup.delete.complete', 'admin');
        } else {
            $app->addError('admin.plugin.faqgroup.delete.error', 'admin');
        }

        return $app->redirect($app->url('admin_shiro8_faq', array('faq_group_id' => $FaqGroup->getId())));
    }

    public function moveRank(Application $app, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $ranks = $request->request->all();
            foreach ($ranks as $faqGroupsId => $rank) {
                $FaqGroup = $app['eccube.plugin.shiro8_faq3.repository.shiro8_faq']
                    ->find($faqGroupsId);
                $FaqGroup->setRank($rank);
                $app['orm.em']->persist($FaqGroup);
            }
            $app['orm.em']->flush();
        }
        return true;
    }
}
