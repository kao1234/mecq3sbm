<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Plugin\ShoppingEx\ServiceProvider;

use Eccube\Application;
use Silex\Application as BaseApplication;
use Silex\ServiceProviderInterface;

class ShoppingExServiceProvider implements ServiceProviderInterface
{
    public function register(BaseApplication $app)
    {
        //$c->match('/help/tradelaw', '\Eccube\Controller\HelpController::tradelaw')->bind('help_tradelaw');
        $app->match('/company' , '\Eccube\Controller\HelpController::tradelaw')->bind('help_tradelaw')
        ->requireHttps()
        ;
        $app->match('/privacy', '\Eccube\Controller\HelpController::privacy')->bind('help_privacy')
        ->requireHttps()
        ;
        $app->match('/help/about' , 'Plugin\ShoppingEx\Controller\RedirectController::index')->bind('help_about')
        ->requireHttps()
        ;
        $app->match('/help/guide' , 'Plugin\ShoppingEx\Controller\RedirectController::index')->bind('help_guide_404')
        ->requireHttps()
        ;
        $app->match('/help/agreement' , 'Plugin\ShoppingEx\Controller\RedirectController::index')->bind('help_agreement')
        ->requireHttps()
        ;

        //$app->match('/about-sim/' , 'Plugin\ShoppingEx\Controller\AboutSimController::index')->bind('about-sim');
        //$c->match('/about-sim', '\Eccube\Controller\UserDataController::index')->bind('aboutsim');

        $app->match('/guide' , '\Eccube\Controller\HelpController::guide')->bind('help_guide')
        ->requireHttps()
        ;
        $app->match('/entry' , 'Plugin\ShoppingEx\Controller\RedirectController::index')->bind('entry')
        ->requireHttps()
        ;
        $app->match('/mypage/login' , 'Plugin\ShoppingEx\Controller\RedirectController::index')->bind('mypage_login')
        ->requireHttps()
        ;
        $app->match('/mypage/favorite' , 'Plugin\ShoppingEx\Controller\RedirectController::index')->bind('mypage_favorite')
        ->requireHttps()
        ;


        // user定義
//        $app->match('/hogehoge', 'Plugin\ShoppingEx\Controller\UserDataController::index')->value('route', 'testpage1')->bind('testpage1');
//        $app->match('/aaa/hogehoge2', 'Plugin\ShoppingEx\Controller\UserDataController::index')->value('route', 'testpage2')->bind('testpage2');

        // ブロック
        $app->match('/block/important_matter_block', '\Plugin\ShoppingEx\Controller\Block\ImportantMatterController::index')
            ->bind('block_important_matter_block');

        foreach($this->getRedirectTo() as $k=>$v){
            $app->match($k , 'Plugin\ShoppingEx\Controller\RedirectController::index')
            ->requireHttps()
            ;

        }

        // Form/Type
        $app['form.types'] = $app->share($app->extend('form.types', function ($types) use($app) {
            $types[] = new \Plugin\ShoppingEx\Form\Type\CardNoType($app['config']);
            $types[] = new \Plugin\ShoppingEx\Form\Type\CardLimitType($app['config']);
            $types[] = new \Plugin\ShoppingEx\Form\Type\CardTypeType($app['config']);
            $types[] = new \Plugin\ShoppingEx\Form\Type\CardSecType($app['config']);
            $types[] = new \Plugin\ShoppingEx\Form\Type\CardFormType($app);
            return $types;
        }));


        // Form/Extension
        $app['form.type.extensions'] = $app->share($app->extend('form.type.extensions', function ($extensions) {
            $extensions[] = new \Plugin\ShoppingEx\Form\Extension\ShoppingExExtension();
            return $extensions;
        }));

        //Repository
        $app['shoppingex.repository.shoppingex'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Plugin\ShoppingEx\Entity\ShoppingEx');
        });
        $app['shoppingex.repository.shoppingexcleanup'] = $app->share(function () use ($app) {
            return $app['orm.em']->getRepository('Plugin\ShoppingEx\Entity\ShoppingExCleanup');
        });

        // サービスの登録
        $app['eccube.plugin.shoppingex.service.shoppingex'] = $app->share(function () use ($app) {
            return new \Plugin\ShoppingEx\Service\ShoppingExService($app);
        });

        $app['eccube.plugin.shoppingex.service.shoppingex']->setRedirectTo($this->getRedirectTo());
    }

    public function boot(BaseApplication $app)
    {
    }
    public function getRedirectTo(){
        $redirectto = array(
            '/guide.php'=>'/guide'
            ,'/company.php'=>'/company'
            ,'/privacy.php'=>'/privacy'

            ,'/smartphones/' => '/products/list?category=1'
            ,'/smartphones/i5s.php' => '/products/detail/133'
            ,'/smartphones/xperia-xz.php' => '/products/detail/121'
            ,'/smartphones/303sh.php' => '/products/detail/122'
            ,'/mobiles/' => '/products/list?category_id=2'
            ,'/mobiles/301p.php' => ''
            ,'/mobiles/301sh.php' => '/products/detail/127'
            ,'/mobiles/202sh.php' => '/products/detail/128'
            ,'/plan.php' => '/charge'
            ,'/campaign/' => '/campaign'
            ,'/faq.php' => '/faq'
            );

        return $redirectto;

    }
}
