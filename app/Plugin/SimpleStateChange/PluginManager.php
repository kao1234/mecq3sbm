<?php

namespace Plugin\SimpleStateChange;

use Eccube\Plugin\AbstractPluginManager;
use Eccube\Entity\Master\DeviceType;

class PluginManager extends AbstractPluginManager
{

    public function install($config, $app)
    {

    }

    public function uninstall($config, $app)
    {

    }

    public function enable($config, $app)
    {

    }

    public function disable($config, $app)
    {

    }

    public function update($config, $app)
    {
    	
    }
}
