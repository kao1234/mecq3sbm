<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\ProductReview\Controller\Block;

use Eccube\Application;
use Eccube\Common\Constant;

use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Eccube\Util\Str;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;

class ProductReviewBlockController extends AbstractController
{
    private $title;

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionKey = 'eccube.front.shopping.nonmember';

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionCustomerAddressKey = 'eccube.front.shopping.nonmember.customeraddress';

    /**
     * @var string 複数配送警告メッセージ
     */
    private $sessionMultipleKey = 'eccube.front.shopping.multiple';

    /**
     * @var string 受注IDキー
     */
    private $sessionOrderKey = 'eccube.front.shopping.order.id';


    public function __construct()
    {
        $this->title = '';
    }


    public function index(Application $app, Request $request)
    {

        $id = $app['request']->attributes->get('id');

        if( !$id ){
            return;
        }

        if ($request->getMethod() === 'GET') {

            $limit = $app['config']['review_regist_max'];
            $id = $app['request']->attributes->get('id');
            $Product = $app['eccube.repository.product']->find($id);
            $Disp = $app['eccube.repository.master.disp']
                ->find(\Eccube\Entity\Master\Disp::DISPLAY_SHOW);
            $ProductReviews = $app['eccube.plugin.product_review.repository.product_review']
                ->findBy(array(
                    'Product' => $Product,
                    'Status' => $Disp
                ),
                array('create_date' => 'DESC'),
                $limit === null ? 5 : $limit
            );

            $twig = $app->renderView(
                'Block/productreview_block.twig',
                array(
                    'id' => $id,
                    'ProductReviews' => $ProductReviews,
                )
            );
        }


        return $twig;
    }


}
