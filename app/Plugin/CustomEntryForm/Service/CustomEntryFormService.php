<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2016 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Plugin\CustomEntryForm\Service;

use Eccube\Application;

class CustomEntryFormService
{
    /**
     * @var Application
     */
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function createTagOrderKey(){
    	$orderid = "".date("ymdHis").sprintf("%02d",mt_rand(1,99));

    	$this->app['session']->set('tag.eccube.tagplugin.orderkey',$orderid);
    }

    public function getTagOrderKey(){

    	return $this->app['session']->get('tag.eccube.tagplugin.orderkey');
    }
    public function removeTagOrderKey(){

    	$this->app['session']->remove('tag.eccube.tagplugin.orderkey');
    }

    public function getA8TagOrderCode(){
        $service = $this->app['eccube.plugin.fdroute.service.fdroute'];

        if($service){
            if($service->isA8()){
                
                $tagorder = $this->app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->getTagOrderKey();
                return $tagorder;

            }


        }
        return null;



    }
}