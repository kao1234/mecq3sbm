<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Plugin\CustomEntryForm\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContext;

class UseCarrierType extends AbstractType
{

    public $config;

    public function __construct(
        $config
    ) {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder->add('usecarrier', 'choice', array(
            'required'=>false,
            'choices' => array(
                'au'=>'au',
                'docomo'=>'docomo',
                'softbank'=>'softbank',
                'その他'=>'その他',
                '未利用'=>'未利用'
                ),
            'empty_value' => false,            
            'multiple' => false,
            'expanded'   => true,
        ));


    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // $resolver->setRequired('product');
        $resolver->setDefaults(array(
            'expanded' => true,
            'constraints' => array(
                new Assert\Callback(array($this, 'validate')),
            ),
        ));
    }

    /*
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        // if ($options['id_add_product_id']) {
        //     foreach ($view->vars['form']->children as $child) {
        //         $child->vars['id'] .= $options['product']->getId();
        //     }
        // }

        // if ($view->vars['form']->children['mode']->vars['value'] === 'customentryform_add_cart') {
        //     $view->vars['form']->children['mode']->vars['value'] = '';
        // }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'usecarrier';
    }

    /**
     * validate
     *
     * @param type             $data
     * @param ExecutionContext $context
     */
    public function validate($data, ExecutionContext $context)
    {
        if ($data['mode'] !== 'add_favorite') {
            // $context->validateValue($data['product_class_id'], array(
            //     new Assert\NotBlank(),
            // ), '[product_class_id]');
            // if ($this->Product->getClassName1()) {
            //     $context->validateValue($data['classcategory_id1'], array(
            //         new Assert\NotBlank(),
            //         new Assert\NotEqualTo(array(
            //             'value' => '__unselected',
            //             'message' => 'form.type.select.notselect'
            //         )),
            //     ), '[classcategory_id1]');
            // }
            // //商品規格2初期状態(未選択)の場合の返却値は「NULL」で「__unselected」ではない
            // if ($this->Product->getClassName2()) {
            //     $context->validateValue($data['classcategory_id2'], array(
            //         new Assert\NotBlank(),
            //         new Assert\NotEqualTo(array(
            //             'value' => '__unselected',
            //             'message' => 'form.type.select.notselect'
            //         )),
            //     ), '[classcategory_id2]');
            // }

        }
    }
}
