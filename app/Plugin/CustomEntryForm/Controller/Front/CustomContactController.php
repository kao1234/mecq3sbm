<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


namespace Plugin\CustomEntryForm\Controller\Front;

use Eccube\Application;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Symfony\Component\HttpFoundation\Request;

class CustomContactController
{

    private $forminfokey = 'eccube.plugin.customentryform.contact.forminfo';

    /**
     * お問い合わせ画面.
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Application $app, Request $request)
    {
        $builder = $app['form.factory']->createBuilder('customentryform_contact');

        //会員機能を使わないので通らない部分
        if ($app->isGranted('ROLE_USER')) {
            $user = $app['user'];
            $builder->setData(
                array(
                    'name01' => $user->getName01(),
                    'name02' => $user->getName02(),
                    'kana01' => $user->getKana01(),
                    'kana02' => $user->getKana02(),
                    'zip01' => $user->getZip01(),
                    'zip02' => $user->getZip02(),
                    'pref' => $user->getPref(),
                    'addr01' => $user->getAddr01(),
                    'addr02' => $user->getAddr02(),
                    'tel01' => $user->getTel01(),
                    'tel02' => $user->getTel02(),
                    'tel03' => $user->getTel03(),
                    'email' => $user->getEmail(),
                )
            );
        }

        // FRONT_CONTACT_INDEX_INITIALIZE
        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CONTACT_INDEX_INITIALIZE, $event);

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $BaseInfo = $app['eccube.repository.base_info']->get();
            switch ($request->get('mode')) {
                case 'back':
                    $data = $form->getData();
                    $app['session']->set($this->forminfokey,$data);

                    return $app->redirect($app->url('plugin_customentryform_forminquiry'));


                case 'confirm':
                    $builder->setAttribute('freeze', true);
                    $form = $builder->getForm();
                    $form->handleRequest($request);

                    return $app->render(
                        'CustomContactForm/confirm.twig',
                        array(
                        'form' => $form->createView(),
                    ));

                case 'complete':

                    $data = $form->getData();

                    $event = new EventArgs(
                        array(
                            'form' => $form,
                            'data' => $data,
                            'formtype'=>'inquiry',
                        ),
                        $request
                    );
                    // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CONTACT_INDEX_COMPLETE, $event);
                    //タグ用のオーダー番号生成
                    $app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->createTagOrderKey();

                    $data = $event->getArgument('data');

                    $msg = $data['contents'];
                    $a8tag = $app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->getA8TagOrderCode();
                    if($a8tag){
                        $msg .= "【a8注文番号】sbm".$a8tag;

                    }
                    $data['contents'] = $msg;
                    $event->setArgument('data',$data);
                    //ＤＢ送信
                    $app['eccube.plugin.shoppingex.service.shoppingex']->sendContact($event);
                    // メール送信
                    if($data['email']){
                    }else{
                        $data['email'] = $BaseInfo->getEmail01();
                    
                    }
                    $app['eccube.service.mail']->sendContactMail($data);

                    $app['session']->remove($this->forminfokey);
                    return $app->redirect($app->url('plugin_customentryform_forminquiry_complete'));
            }
        }else{

            
            $data = $app['session']->get($this->forminfokey);

            if($data){
                if($data['pref']){
                    $prefid = $data['pref']->getId();
                    $pref = $app['eccube.repository.master.pref']->find($prefid);
                    $data['pref']= $pref;

                }

                $form->setData($data);

            }
        }
        return $app->render(
            'CustomContactForm/index.twig',
            array(
                'form' => $form->createView(),

            )
        );

    }

    /**
     * お問い合わせ完了画面.
     *
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function complete(Application $app)
    {




        return $app->render(
            'CustomContactForm/complete.twig'
            );
    }
}
