<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\CustomEntryForm\Controller\Front;

use Eccube\Application;
use Eccube\Common\Constant;

use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Eccube\Util\Str;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;
use Eccube\Exception\ShoppingException;

use Eccube\Entity\Customer;
use Eccube\Entity\CustomerAddress;


class CustomEntryFormController extends AbstractController
{
    private $title;

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionKey = 'eccube.front.shopping.nonmember';

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionCustomerAddressKey = 'eccube.front.shopping.nonmember.customeraddress';

    /**
     * @var string 複数配送警告メッセージ
     */
    private $sessionMultipleKey = 'eccube.front.shopping.multiple';

    /**
     * @var string 受注IDキー
     */
    private $sessionOrderKey = 'eccube.front.shopping.order.id';


    public function __construct()
    {
        $this->title = '';
    }

    //通常
    public function webentry(Application $app, Request $request,$id=null){
        return $this->index($app,$request,$id,'webentry','CustomEntryForm');
    }
    //店舗受け取り
    public function shopentry(Application $app, Request $request,$id=null){
        return $this->index($app,$request,$id,'shopentry','CustomShopEntryForm');
    }

    public function index(Application $app, Request $request,$id=null,$formtype=null,$twigpath=null)
    {

        //カーと入れる
        //bystepでカーとロック

        //お客様情報入力

        //申込確認

        //完了

        $Cart = $app['eccube.service.cart']->getCart();

        // FRONT_CART_INDEX_INITIALIZE
        // $event = new EventArgs(
        //     array(),
        //     $request
        // );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CART_INDEX_INITIALIZE, $event);

        /* @var $BaseInfo \Eccube\Entity\BaseInfo */
        /* @var $Cart \Eccube\Entity\Cart */
        $BaseInfo = $app['eccube.repository.base_info']->get();

        $isDeliveryFree = false;
        $least = 0;
        $quantity = 0;
        if ($BaseInfo->getDeliveryFreeAmount()) {
            if ($BaseInfo->getDeliveryFreeAmount() <= $Cart->getTotalPrice()) {
                // 送料無料（金額）を超えている
                $isDeliveryFree = true;
            } else {
                $least = $BaseInfo->getDeliveryFreeAmount() - $Cart->getTotalPrice();
            }
        }

        if ($BaseInfo->getDeliveryFreeQuantity()) {
            if ($BaseInfo->getDeliveryFreeQuantity() <= $Cart->getTotalQuantity()) {
                // 送料無料（個数）を超えている
                $isDeliveryFree = true;
            } else {
                $quantity = $BaseInfo->getDeliveryFreeQuantity() - $Cart->getTotalQuantity();
            }
        }

        // FRONT_CART_INDEX_COMPLETE
        // $event = new EventArgs(
        //     array(),
        //     $request
        // );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CART_INDEX_COMPLETE, $event);

        // if ($event->hasResponse()) {
        //     return $event->getResponse();
        // }


        $cartService = $app['eccube.service.cart'];
        /*
                // カートチェック
                if (!$cartService->isLocked()) {
                    // カートが存在しない、カートがロックされていない時はエラー
                    log_info('カートが存在しません');
                    return $app->redirect($app->url('cart'));
                }

                // ログイン済みの場合は, 購入画面へリダイレクト.
                if ($app->isGranted('ROLE_USER')) {
                    return $app->redirect($app->url('shopping'));
                }
        */
        // カートチェック
        if (count($cartService->getCart()->getCartItems()) <= 0) {
            // カートが存在しない時はエラー
            log_info('カートに商品が入っていないためトップ画面にリダイレクト');
            return $app->redirect($app->url('homepage'));
        }

        $Product = $app['eccube.repository.product']->get($id);
        foreach($cartService->getCart()->getCartItems() as $p){
            $cartpid = $p->getObject()->getProduct()->getId();

        }
        if ($id <> $cartpid) {
            # code...
            log_info('不正な呼び出しのためnotfound');
            throw new NotFoundHttpException();

        }


            // FRONT_CART_BUYSTEP_INITIALIZE
            // $event = new EventArgs(
            //     array(),
            //     $request
            // );
            // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CART_BUYSTEP_INITIALIZE, $event);

        $app['eccube.service.cart']->lock();
        $app['eccube.service.cart']->save();

            // FRONT_CART_BUYSTEP_COMPLETE
            // $event = new EventArgs(
            //     array(),
            //     $request
            // );
            // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CART_BUYSTEP_COMPLETE, $event);

            // if ($event->hasResponse()) {
            //     return $event->getResponse();
            // }


        $builder = $app['form.factory']->createBuilder('customentryform_nonmember');

        // $builder
        //     ->remove('name.name02')
        //     ->remove('kana.kana02')
        //     ->remove('sex')
        //     ->remove('company_name')
        //     ->remove('zip')
        //     ->remove('address')
        // ;
        $builder
            ->add('id', 'hidden', array(
                'required' => false,
                'label' => 'id',
                'data' => $id,
            ))
        ;

        //初期値表示用
        $form = $builder->getForm();

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'form'=>$form,
            ),
            $request
        );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_NONMEMBER_INITIALIZE, $event);
        // if($event->getArgument('form')){
        //     $form = $event->getArgument('form');
        // }


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            log_info('非会員お客様情報登録開始');

            $data = $form->getData();
            $Customer = new Customer();
            $Customer
                ->setName01($data['name01'])
                ->setName02($data['name02'])
                ->setKana01($data['kana01'])
                ->setKana02($data['kana02'])
                // ->setSex($data['sex'])

                // ->setCompanyName($data['company_name'])
                ->setEmail($data['email'])
                ->setTel01($data['tel01'])
                ->setTel02($data['tel02'])
                ->setTel03($data['tel03'])
                ->setZip01($data['zip01'])
                ->setZip02($data['zip02'])
                ->setZipCode($data['zip01'].$data['zip02'])
                ->setPref($data['pref'])
                ->setAddr01($data['addr01'])
                ->setAddr02($data['addr02'])
                ;

            // 非会員複数配送用
            $CustomerAddress = new CustomerAddress();
            $CustomerAddress
                ->setCustomer($Customer)
                ->setName01($data['name01'])
                ->setName02($data['name02'])
                ->setKana01($data['kana01'])
                ->setKana02($data['kana02'])
                // ->setCompanyName($data['company_name'])
                ->setTel01($data['tel01'])
                ->setTel02($data['tel02'])
                ->setTel03($data['tel03'])
                ->setZip01($data['zip01'])
                ->setZip02($data['zip02'])
                ->setZipCode($data['zip01'].$data['zip02'])
                ->setPref($data['pref'])
                ->setAddr01($data['addr01'])
                ->setAddr02($data['addr02'])
                ->setDelFlg(Constant::DISABLED);
            $Customer->addCustomerAddress($CustomerAddress);

            // 受注情報を取得
            $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);

            // 初回アクセス(受注データがない)の場合は, 受注情報を作成
            if (is_null($Order)) {
                // 受注情報を作成
                try {
                    // 受注情報を作成
                    $Order = $app['eccube.service.shopping']->createOrder($Customer);
                } catch (CartException $e) {
                    $app->addRequestError($e->getMessage());
                    return $app->redirect($app->url('homepage'));
                }
            }

            // 非会員用セッションを作成
            $nonMember = array();
            $nonMember['customer'] = $Customer;
            $nonMember['pref'] = $Customer->getPref()->getId();
            $nonMember['id'] = $id;
            // $nonMember['sex'] = $Customer->getSex();
            $app['session']->set($this->sessionKey, $nonMember);

            $customerAddresses = array();
            $customerAddresses[] = $CustomerAddress;
            $app['session']->set($this->sessionCustomerAddressKey, serialize($customerAddresses));

            $app['session']->set('customnonmember_nonmember',array($form->getData()));

            // $event = new EventArgs(
            //     array(
            //         'form' => $form,
            //         'Order' => $Order,
            //     ),
            //     $request
            // );
            // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_NONMEMBER_COMPLETE, $event);

            // if ($event->getResponse() !== null) {
            //     return $event->getResponse();
            // }

            log_info('非会員お客様情報登録完了', array($Order->getId()));

            return $app->redirect($app->url('plugin_customentryform_form'.$formtype.'_confirm',array('id'=>$id)
              ));
        }

        $pcolors = $app['eccube.plugin.productcolor.repository.product_productcolor']->findProductColor($Product);

        return $app->render(
            ''.$twigpath.'/index.twig',
            array(
                'Cart' => $Cart,
                'least' => $least,
                'quantity' => $quantity,
                'is_delivery_free' => $isDeliveryFree,
                'form' => $form->createView(),
                'id' => $id,
                'ProductColor'=>$pcolors,


            )
        );
    }

    public function formwebentry_confirm(Application $app, Request $request,$id=null){
        return $this->form_confirm($app,$request,$id,'webentry','CustomEntryForm');

    }
    public function formshopentry_confirm(Application $app, Request $request,$id=null){
        return $this->form_confirm($app,$request,$id,'shopentry','CustomShopEntryForm');

    }


    public function form_confirm(Application $app, Request $request,$id=null,$formtype=null,$twigpath=null)
    {
        $sec=$app['session'];
        $formdata = $sec->get('customnonmember_nonmember');

        $Cart = $app['eccube.service.cart']->getCart();

        // FRONT_CART_INDEX_INITIALIZE
        // $event = new EventArgs(
        //     array(),
        //     $request
        // );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_CART_INDEX_INITIALIZE, $event);

        /* @var $BaseInfo \Eccube\Entity\BaseInfo */
        /* @var $Cart \Eccube\Entity\Cart */
        $BaseInfo = $app['eccube.repository.base_info']->get();

        $cartService = $app['eccube.service.cart'];
        /*
                // カートチェック
                if (!$cartService->isLocked()) {
                    // カートが存在しない、カートがロックされていない時はエラー
                    log_info('カートが存在しません');
                    return $app->redirect($app->url('cart'));
                }

                // ログイン済みの場合は, 購入画面へリダイレクト.
                if ($app->isGranted('ROLE_USER')) {
                    return $app->redirect($app->url('shopping'));
                }
        */
        // カートチェック
        if (count($cartService->getCart()->getCartItems()) <= 0) {
            // カートが存在しない時はエラー
            log_info('カートに商品が入っていないためトップにリダイレクト');
            return $app->redirect($app->url('homepage'));
        }

        if ($id && $formtype && $twigpath) {

        }else{
            // カートが存在しない時はエラー
            log_info('呼出しが不正なためトップ画面にリダイレクト');
            return $app->redirect($app->url('homepage'));
        }
        $formdata = $sec->get('customnonmember_nonmember');

        if(!$formdata[0]['id']){
            // カートが存在しない時はエラー
            log_info('呼出しが不正なためトップ画面にリダイレクト');
            return $app->redirect($app->url('homepage'));

        }else{
            $id = $formdata[0]['id'];
        }


        $Product = $app['eccube.repository.product']->get($id);
        foreach($cartService->getCart()->getCartItems() as $p){
            $cartpid = $p->getObject()->getProduct()->getId();

        }
        if ($id <> $cartpid) {
            # code...
            log_info('不正な呼び出しのためnotfound');
            throw new NotFoundHttpException();

        }




        $formdata = $sec->get('customnonmember_nonmember');


        $builder = $app['form.factory']->createBuilder('customentryform_nonmember');

        $builder
            ->add('id', 'hidden', array(
                'required' => false,
                'label' => 'id',
                'data' => $id,
            ))
        ;
        $builder->setAttribute('freeze', true);

        if($formdata){
            $form = $builder->getForm();
            //本体で参照わたしでいれてる（本体のカスタマイズ）。初期値設定のため。
            //$form = $event->getArgument('form');

            $prefid = $formdata[0]['pref'];
            $pref = $app['eccube.repository.master.pref']->find($prefid);
            $formdata[0]['pref']= $pref;
            $dt = $formdata[0];

            $form->setData($dt);
        }

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'form'=>$form,
            ),
            $request
        );

        // $form->handleRequest($request);


        //初期値表示用
        // $form = $builder->getForm();

        // $event = new EventArgs(
        //     array(
        //         'builder' => $builder,
        //         'form'=>$form,
        //     ),
        //     $request
        // );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_NONMEMBER_INITIALIZE, $event);
        // if($event->getArgument('form')){
        //     $form = $event->getArgument('form');
        // }


        $cartService = $app['eccube.service.cart'];

        // カートチェック
        if (!$cartService->isLocked()) {
            log_info('カートが存在しません');
            // カートが存在しない、カートがロックされていない時はエラー
            return $app->redirect($app->url('homepage'));
        }

        // カートチェック
        if (count($cartService->getCart()->getCartItems()) <= 0) {
            log_info('カートに商品が入っていないためトップ画面にリダイレクト');
            // カートが存在しない時はエラー
            return $app->redirect($app->url('homepage'));
        }

        // 登録済みの受注情報を取得
        $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);

        // 初回アクセス(受注情報がない)の場合は, 受注情報を作成
        if (is_null($Order)) {
            // 未ログインの場合, ログイン画面へリダイレクト.
            if (!$app->isGranted('IS_AUTHENTICATED_FULLY')) {
                // 非会員でも一度会員登録されていればショッピング画面へ遷移
                $Customer = $app['eccube.service.shopping']->getNonMember($this->sessionKey);

                if (is_null($Customer)) {
                    log_info('未ログインのためログイン画面にリダイレクト');
                    return $app->redirect($app->url('homepage'));
                }
            } else {
                $Customer = $app->user();
            }

            try {
                // 受注情報を作成
                $Order = $app['eccube.service.shopping']->createOrder($Customer);
            } catch (CartException $e) {
                log_error('初回受注情報作成エラー', array($e->getMessage()));
                $app->addRequestError($e->getMessage());
                return $app->redirect($app->url('plugin_customentryform_form'.$formtype.'_confirm',array('id'=>$id)
                  ));
            }

            // セッション情報を削除
            $app['session']->remove($this->sessionOrderKey);
            $app['session']->remove($this->sessionMultipleKey);
        }

        // 受注関連情報を最新状態に更新
        $app['orm.em']->refresh($Order);


        $pcolors = $app['eccube.plugin.productcolor.repository.product_productcolor']->findProductColor($Product);
        
        $ordermsg = null;

        foreach($Cart->getCartItems() as $CartItem){
            if($CartItem->getColor()){
                $ordermsg .= '【カラー】'.$pcolors[$CartItem->getColor()]['name'].'/';

            }else{
                $ordermsg .= '【カラー】相談してきめる/';
            }

        }
        $ordermsg .= '【利用中キャリア】'.$formdata[0]['usecarrier']['usecarrier'].'/';
        if($formdata[0]['contents']){
            $ordermsg .= '【店舗受取】'.$formdata[0]['contents'].'/';

        }

        $Order->setMessage($ordermsg);

        // form作成
        $builder = $app['eccube.service.shopping']->getShippingFormBuilder($Order);

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Order' => $Order,
            ),
            $request
        );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_INDEX_INITIALIZE, $event);

        $formShopping = $builder->getForm();

        if ($Order->getTotalPrice() < 0) {
            // 合計金額がマイナスの場合、エラー
            log_info('受注金額マイナスエラー', array($Order->getId()));
            $message = $app->trans('shopping.total.price', array('totalPrice' => number_format($Order->getTotalPrice())));
            $app->addError($message);

            return $app->redirect($app->url('shopping_error'));
        }

        // 複数配送の場合、エラーメッセージを一度だけ表示
        if (!$app['session']->has($this->sessionMultipleKey)) {
            if (count($Order->getShippings()) > 1) {
                $app->addRequestError('shopping.multiple.delivery');
            }
            $app['session']->set($this->sessionMultipleKey, 'multiple');
        }


        return $app->render(
            ''.$twigpath.'/confirm.twig',
            array(
                'Cart' => $Cart,
                'least' => $least,
                'Order' => $Order,
                'quantity' => $quantity,
                'is_delivery_free' => $isDeliveryFree,
                'formtype' => $formtype,
                'form' => $form->createView(),
                'formShopping' => $formShopping->createView(),
                'id' => $id,
                'ProductColor'=>$pcolors,

            )
        );
    }

    public function list_index_custom(Application $app, Request $request)
    {
        $BaseInfo = $app['eccube.repository.base_info']->get();

        // Doctrine SQLFilter
        if ($BaseInfo->getNostockHidden() === Constant::ENABLED) {
            $app['orm.em']->getFilters()->enable('nostock_hidden');
        }

        // handleRequestは空のqueryの場合は無視するため
        if ($request->getMethod() === 'GET') {
            $request->query->set('pageno', $request->query->get('pageno', ''));
        }

        // searchForm
        /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
        $builder = $app['form.factory']->createNamedBuilder('', 'search_product');
        $builder->setAttribute('freeze', true);
        $builder->setAttribute('freeze_display_text', false);
        if ($request->getMethod() === 'GET') {
            $builder->setMethod('GET');
        }

        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_INDEX_INITIALIZE, $event);

        /* @var $searchForm \Symfony\Component\Form\FormInterface */
        $searchForm = $builder->getForm();

        $searchForm->handleRequest($request);

        // paginator
        $searchData = $searchForm->getData();
        $qb = $app['eccube.repository.product']->getQueryBuilderBySearchData($searchData);

        $event = new EventArgs(
            array(
                'searchData' => $searchData,
                'qb' => $qb,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_INDEX_SEARCH, $event);
        $searchData = $event->getArgument('searchData');

        //sort=xxがqueryに入るとシステムエラーになる回避用 doctrineのバグ
        try{

            $pagination = $app['paginator']()->paginate(
                $qb,
                !empty($searchData['pageno']) ? $searchData['pageno'] : 1,
                $searchData['disp_number']->getId()
            );
        } catch (\Exception $e) {
            throw new NotFoundHttpException();
        }

        // addCart form
        $forms = array();
        foreach ($pagination as $Product) {
            /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
            $builder = $app['form.factory']->createNamedBuilder('', 'add_cart', null, array(
                'product' => $Product,
                'allow_extra_fields' => true,
            ));
            $addCartForm = $builder->getForm();

            if ($request->getMethod() === 'POST' && (string)$Product->getId() === $request->get('product_id')) {
                $addCartForm->handleRequest($request);

                if ($addCartForm->isValid()) {
                    $addCartData = $addCartForm->getData();

                    try {
                        $app['eccube.service.cart']->addProduct($addCartData['product_class_id'], $addCartData['quantity'])->save();
                    } catch (CartException $e) {
                        $app->addRequestError($e->getMessage());
                    }

                    $event = new EventArgs(
                        array(
                            'form' => $addCartForm,
                            'Product' => $Product,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_INDEX_COMPLETE, $event);

                    if ($event->getResponse() !== null) {
                        return $event->getResponse();
                    }

                    return $app->redirect($app->url('cart'));
                }
            }

            $forms[$Product->getId()] = $addCartForm->createView();
        }

        // 表示件数
        $builder = $app['form.factory']->createNamedBuilder('disp_number', 'product_list_max', null, array(
            'empty_data' => null,
            'required' => false,
            'label' => '表示件数',
            'allow_extra_fields' => true,
        ));
        if ($request->getMethod() === 'GET') {
            $builder->setMethod('GET');
        }

        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_INDEX_DISP, $event);
        $dispNumberForm = $builder->getForm();

        $dispNumberForm->handleRequest($request);

        // ソート順
        $builder = $app['form.factory']->createNamedBuilder('orderby', 'product_list_order_by', null, array(
            'empty_data' => null,
            'required' => false,
            'label' => '表示順',
            'allow_extra_fields' => true,
        ));
        if ($request->getMethod() === 'GET') {
            $builder->setMethod('GET');
        }

        $event = new EventArgs(
            array(
                'builder' => $builder,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_INDEX_ORDER, $event);

        $orderByForm = $builder->getForm();

        $orderByForm->handleRequest($request);

        $Category = $searchForm->get('category_id')->getData();

        return $app->render('Product/list.twig', array(
            'subtitle' => $this->getPageTitle($searchData),
            'pagination' => $pagination,
            'search_form' => $searchForm->createView(),
            'disp_number_form' => $dispNumberForm->createView(),
            'order_by_form' => $orderByForm->createView(),
            'forms' => $forms,
            'Category' => $Category,
        ));
    }



    public function detail_custom(Application $app, Request $request, $id)
    {

        $BaseInfo = $app['eccube.repository.base_info']->get();
        if ($BaseInfo->getNostockHidden() === Constant::ENABLED) {
            $app['orm.em']->getFilters()->enable('nostock_hidden');
        }

        /* @var $Product \Eccube\Entity\Product */
        $Product = $app['eccube.repository.product']->get($id);

        if (!$request->getSession()->has('_security_admin') && $Product->getStatus()->getId() !== 1) {
            throw new NotFoundHttpException();
        }
        if (count($Product->getProductClasses()) < 1) {
            throw new NotFoundHttpException();
        }

        $pcolors = $app['eccube.plugin.productcolor.repository.product_productcolor']->findProductColor($Product);

        /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
        $builder = $app['form.factory']->createNamedBuilder('', 'customentryform_add_cart', null, array(
            'product' => $Product,
            'id_add_product_id' => false,
        ));

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Product' => $Product,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_INITIALIZE, $event);
        // $builder->remove('classcategory_id1');
        // $builder->remove('classcategory_id2');



        /* @var $form \Symfony\Component\Form\FormInterface */
        $form = $builder->getForm();

        if ($request->getMethod() === 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {


                $addCartData = $form->getData();


                if ($addCartData['mode'] === 'add_favorite') {
                    // if ($app->isGranted('ROLE_USER')) {
                    //     $Customer = $app->user();
                    //     $app['eccube.repository.customer_favorite_product']->addFavorite($Customer, $Product);
                    //     $app['session']->getFlashBag()->set('product_detail.just_added_favorite', $Product->getId());

                    //     $event = new EventArgs(
                    //         array(
                    //             'form' => $form,
                    //             'Product' => $Product,
                    //         ),
                    //         $request
                    //     );
                    //     $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_FAVORITE, $event);

                    //     if ($event->getResponse() !== null) {
                    //         return $event->getResponse();
                    //     }

                    //     return $app->redirect($app->url('product_detail', array('id' => $Product->getId())));
                    // } else {
                    //     // 非会員の場合、ログイン画面を表示
                    //     //  ログイン後の画面遷移先を設定
                    //     $app->setLoginTargetPath($app->url('product_detail', array('id' => $Product->getId())));
                    //     $app['session']->getFlashBag()->set('eccube.add.favorite', true);
                    //     return $app->redirect($app->url('mypage_login'));
                    // }
                } elseif (in_array($addCartData['mode'],array('customentryform_add_cart','customentryform_add_cart_shop'))
                    ) {
                    $formtype = array('customentryform_add_cart'=>'web','customentryform_add_cart_shop'=>'shop');



                    log_info('カート追加処理開始', array('product_id' => $Product->getId(), 'product_class_id' => $addCartData['product_class_id'], 'quantity' => $addCartData['quantity']));

                    try {
                        $Cart = $app['eccube.service.cart']->getCart();

                        if($Cart){
                            $removed=false;
                            foreach($Cart->getCartItems() as $CartItem){

                                $ProductClass = $CartItem->getObject();
                                if ($ProductClass) {

                                    $app['eccube.service.cart']->removeProduct($ProductClass->getId());
                                    $removed=true;
                                }
                            }
                            if($removed){
                                $app['eccube.service.cart']->save();

                            }

                        }


                        $app['eccube.service.cart']->addProduct($addCartData['product_class_id'], $addCartData['quantity'],$addCartData)->save();
                    } catch (CartException $e) {
                        log_info('カート追加エラー', array($e->getMessage()));
                        $app->addRequestError($e->getMessage());
                    }

                    log_info('カート追加処理完了', array('product_id' => $Product->getId(), 'product_class_id' => $addCartData['product_class_id'], 'quantity' => $addCartData['quantity']));

                    $event = new EventArgs(
                        array(
                            'form' => $form,
                            'Product' => $Product,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_COMPLETE, $event);

                    if ($event->getResponse() !== null) {
                        return $event->getResponse();
                    }

                    return $app->redirect($app->url('plugin_customentryform_form'.$formtype[$addCartData['mode']].'entry',array('id'=>$id)));
                }
            }
        } else {
            $addFavorite = $app['session']->getFlashBag()->get('eccube.add.favorite');
            if (!empty($addFavorite)) {
                // お気に入り登録時にログインされていない場合、ログイン後にお気に入り追加処理を行う
                if ($app->isGranted('ROLE_USER')) {
                    $Customer = $app->user();
                    $app['eccube.repository.customer_favorite_product']->addFavorite($Customer, $Product);
                    $app['session']->getFlashBag()->set('product_detail.just_added_favorite', $Product->getId());
                }
            }


        }
        /* @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $app['orm.em']->getRepository('Plugin\TagEx\\Entity\TagEx')->createQueryBuilder('tagex');

        $qb
            ->innerjoin('tagex.Tag','tag')
            ->innerjoin('tag.ProductTag','pt')
            ->innerjoin('pt.Product','p')
            ->AndWhere('p.id=:product_id')
            ->setParameter(':product_id', $Product->getId())
            ->orderBy('tag.rank', 'desc');

            // ->innerJoin('pt.Tag', 'tag')
            // ->andWhere('p.id = :product_id')
            // ->setParameter(':product_id', $Product->getId())
            // ->orderBy('tag.rank', 'desc');

        $ProductTags = $qb->getQuery()->getResult();

        $is_favorite = false;
        if ($app->isGranted('ROLE_USER')) {
            $Customer = $app->user();
            $is_favorite = $app['eccube.repository.customer_favorite_product']->isFavorite($Customer, $Product);
        }

        return $app->render('Product/detail.twig', array(
            'title' => $this->title,
            'subtitle' => $Product->getName(),
            'form' => $form->createView(),
            'form2' => $form->createView(),
            'Product' => $Product,
            'is_favorite' => $is_favorite,
            'ProductColor'=>$pcolors,
            'ProductTags'=>$ProductTags,
            'BaseInfo'=>$BaseInfo,
        ));
    }

    /**
     * 購入画面表示
     *
     * @param Application $app
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function shopping_index(Application $app, Request $request)
    {


        return $app->render('Shopping/index.twig', array(
            'form' => $form->createView(),
            'Order' => $Order,
        ));
    }


    public function shopping_formwebentry_confirm(Application $app, Request $request,$id=null){
        return $this->shopping_confirm($app,$request,$id,'webentry','CustomEntryForm');

    }
    public function shopping_formshopentry_confirm(Application $app, Request $request,$id=null){
        return $this->shopping_confirm($app,$request,$id,'shopentry','CustomShopEntryForm');

    }
    /**
     * 購入処理
     */
    public function shopping_confirm(Application $app, Request $request,$id=null,$formtype=null,$twigpath=null)
    {
        $id = $request->get('id');
        $BaseInfo = $app['eccube.repository.base_info']->get();


        $Product = $app['eccube.repository.product']->get($id);

        $pcolors = $app['eccube.plugin.productcolor.repository.product_productcolor']->findProductColor($Product);




        $cartService = $app['eccube.service.cart'];

        // カートチェック
        if (!$cartService->isLocked()) {
            // カートが存在しない、カートがロックされていない時はエラー
            log_info('カートが存在しません');
            return $app->redirect($app->url('homepage'));
        }
        $Cart = $app['eccube.service.cart']->getCart();

        $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);
        if (!$Order) {
            log_info('購入処理中の受注情報がないため購入エラー');
            $app->addError('front.shopping.order.error');
            return $app->redirect($app->url('form_error'));
        }

        if ('POST' !== $request->getMethod()) {
            return $app->redirect($app->url('homepage'));
        }

        // form作成
        $builder = $app['eccube.service.shopping']->getShippingFormBuilder($Order);

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Order' => $Order,
                'id' => $id,
                'formtype' => $formtype,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_CONFIRM_INITIALIZE, $event);
        
        $form = $builder->getForm();

        $form->handleRequest($request);
        if ($event->getResponse() !== null) {
            return $event->getResponse();
        }

        if ($form->isSubmitted() && $form->isValid()) {


            $data = $form->getData();

            log_info('購入処理開始', array($Order->getId()));

            // トランザクション制御
            $em = $app['orm.em'];
            $em->getConnection()->beginTransaction();
            try {

                // お問い合わせ、配送時間などのフォーム項目をセット
                $app['eccube.service.shopping']->setFormData($Order, $data);
                // 購入処理
                $app['eccube.service.shopping']->processPurchase($Order);

                $em->flush();
                $em->getConnection()->commit();

                log_info('購入処理完了', array($Order->getId()));

            } catch (ShoppingException $e) {

                log_error('購入エラー', array($e->getMessage()));

                $em->getConnection()->rollback();

                $app->log($e);
                $app->addError($e->getMessage());

                return $app->redirect($app->url('form_error'));
            } catch (\Exception $e) {

                log_error('予期しないエラー', array($e->getMessage()));

                $em->getConnection()->rollback();

                $app->log($e);

                $app->addError('front.shopping.system.error');
                return $app->redirect($app->url('form_error'));
            }

            // カート削除
            $app['eccube.service.cart']->clear()->save();


            $event = new EventArgs(
                array(
                    'form' => $form,
                    'Order' => $Order,
                    'Cart' => $Cart,
                    'id' => $id,
                    'ProductColor'=>$pcolors,
                    'formtype' => $formtype,
                ),
                $request
            );
            $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_CONFIRM_PROCESSING, $event);

            if ($event->getResponse() !== null) {
                log_info('イベントレスポンス返却', array($Order->getId()));
                return $event->getResponse();
            }

            //タグ用のオーダー番号生成
            $app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->createTagOrderKey();


            $msg = $Order->getMessage();
            $a8tag = $app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->getA8TagOrderCode();
            if($a8tag){
                $msg .= "【a8注文番号】sbm".$a8tag;
                $Order->setMessage($msg);

            }

            // 受注IDをセッションにセット
            $app['session']->set($this->sessionOrderKey, $Order->getId());



            $app['eccube.plugin.shoppingex.service.shoppingex']->sendShoppingOrder($event);

            if($Order->getEmail()){

            }else{
                $Order->setEmail($BaseInfo->getEmail01());

            }

            // メール送信
            $MailHistory = $app['eccube.service.shopping']->sendOrderMail($Order);

            $event = new EventArgs(
                array(
                    'form' => $form,
                    'Order' => $Order,
                    'MailHistory' => $MailHistory,
                    'id' => $id,
                    'formtype' => $formtype,
                ),
                $request
            );
            $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_CONFIRM_COMPLETE, $event);

            if ($event->getResponse() !== null) {
                log_info('イベントレスポンス返却', array($Order->getId()));
                return $event->getResponse();
            }

            // 完了画面表示
            return $app->redirect($app->url('plugin_customentryform_form'.$formtype.'_complete'));
        }

        log_info('購入チェックエラー', array($Order->getId()));

        return $app->redirect($app->url('plugin_customentryform_form'.$formtype.'_confirm',array('id'=>$id)
          ));


    }


    public function shopping_formwebentry_complete(Application $app, Request $request){
        return $this->shopping_complete($app,$request,'webentry','CustomEntryForm');

    }
    public function shopping_formshopentry_complete(Application $app, Request $request){
        return $this->shopping_complete($app,$request,'shopentry','CustomShopEntryForm');

    }
    /**
     * 購入完了画面表示
     */
    public function shopping_complete(Application $app, Request $request,$formtype=null,$twigpath=null)
    {
        // 受注IDを取得
        $orderId = $app['session']->get($this->sessionOrderKey);

        $event = new EventArgs(
            array(
                'orderId' => $orderId,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_COMPLETE_INITIALIZE, $event);

        if ($event->getResponse() !== null) {
            return $event->getResponse();
        }

        // 受注IDセッションを削除
        $app['session']->remove($this->sessionOrderKey);

        log_info('購入処理完了', array($orderId));

        return $app->render(
            ''.$twigpath.'/complete.twig',
            array('orderId'=>$orderId)
        );
    }


    /**
     * お客様情報の変更(非会員)
     */
    public function customer(Application $app, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            try {

                log_info('非会員お客様情報変更処理開始');

                $data = $request->request->all();

                // 入力チェック
                $errors = $this->customerValidation($app, $data);

                foreach ($errors as $error) {
                    if ($error->count() != 0) {
                        log_info('非会員お客様情報変更入力チェックエラー');
                        $response = new Response(json_encode('NG'), 400);
                        $response->headers->set('Content-Type', 'application/json');
                        return $response;
                    }
                }

                $pref = $app['eccube.repository.master.pref']->findOneBy(array('name' => $data['customer_pref']));
                if (!$pref) {
                    log_info('非会員お客様情報変更入力チェックエラー');
                    $response = new Response(json_encode('NG'), 400);
                    $response->headers->set('Content-Type', 'application/json');
                    return $response;
                }

                $Order = $app['eccube.service.shopping']->getOrder($app['config']['order_processing']);
                if (!$Order) {
                    log_info('カートが存在しません');
                    $app->addError('front.shopping.order.error');
                    return $app->redirect($app->url('shopping_error'));
                }

                $Order
                    ->setName01($data['customer_name01'])
                    ->setName02($data['customer_name02'])
                    ->setCompanyName($data['customer_company_name'])
                    ->setTel01($data['customer_tel01'])
                    ->setTel02($data['customer_tel02'])
                    ->setTel03($data['customer_tel03'])
                    ->setZip01($data['customer_zip01'])
                    ->setZip02($data['customer_zip02'])
                    ->setZipCode($data['customer_zip01'].$data['customer_zip02'])
                    ->setPref($pref)
                    ->setAddr01($data['customer_addr01'])
                    ->setAddr02($data['customer_addr02'])
                    ->setEmail($data['customer_email']);

                // 配送先を更新
                $app['orm.em']->flush();

                // 受注関連情報を最新状態に更新
                $app['orm.em']->refresh($Order);

                $event = new EventArgs(
                    array(
                        'Order' => $Order,
                        'data' => $data,
                    ),
                    $request
                );
                $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_CUSTOMER_INITIALIZE, $event);

                log_info('非会員お客様情報変更処理完了', array($Order->getId()));
                $response = new Response(json_encode('OK'));
                $response->headers->set('Content-Type', 'application/json');
            } catch (\Exception $e) {
                log_error('予期しないエラー', array($e->getMessage()));
                $app['monolog']->error($e);

                $response = new Response(json_encode('NG'), 500);
                $response->headers->set('Content-Type', 'application/json');
            }

            return $response;
        }
    }

    /**
     * 購入エラー画面表示
     */
    public function shoppingError(Application $app, Request $request)
    {

        $event = new EventArgs(
            array(),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_SHOPPING_SHIPPING_ERROR_COMPLETE, $event);

        if ($event->getResponse() !== null) {
            return $event->getResponse();
        }

        return $app->render('Shopping/shopping_error.twig');
    }

    /**
     * 非会員でのお客様情報変更時の入力チェック
     *
     * @param Application $app
     * @param array $data リクエストパラメータ
     * @return array
     */
    private function customerValidation(Application $app, array $data)
    {
        // 入力チェック
        $errors = array();

        $errors[] = $app['validator']->validateValue($data['customer_name01'], array(
            new Assert\NotBlank(),
            new Assert\Length(array('max' => $app['config']['name_len'],)),
            new Assert\Regex(array('pattern' => '/^[^\s ]+$/u', 'message' => 'form.type.name.firstname.nothasspace'))
        ));

        $errors[] = $app['validator']->validateValue($data['customer_name02'], array(
            new Assert\NotBlank(),
            new Assert\Length(array('max' => $app['config']['name_len'],)),
            new Assert\Regex(array('pattern' => '/^[^\s ]+$/u', 'message' => 'form.type.name.firstname.nothasspace'))
        ));

        $errors[] = $app['validator']->validateValue($data['customer_company_name'], array(
            new Assert\Length(array('max' => $app['config']['stext_len'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_tel01'], array(
            new Assert\NotBlank(),
            new Assert\Type(array('type' => 'numeric', 'message' => 'form.type.numeric.invalid')),
            new Assert\Length(array('max' => $app['config']['tel_len'], 'min' => $app['config']['tel_len_min'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_tel02'], array(
            new Assert\NotBlank(),
            new Assert\Type(array('type' => 'numeric', 'message' => 'form.type.numeric.invalid')),
            new Assert\Length(array('max' => $app['config']['tel_len'], 'min' => $app['config']['tel_len_min'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_tel03'], array(
            new Assert\NotBlank(),
            new Assert\Type(array('type' => 'numeric', 'message' => 'form.type.numeric.invalid')),
            new Assert\Length(array('max' => $app['config']['tel_len'], 'min' => $app['config']['tel_len_min'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_zip01'], array(
            new Assert\NotBlank(),
            new Assert\Type(array('type' => 'numeric', 'message' => 'form.type.numeric.invalid')),
            new Assert\Length(array('min' => $app['config']['zip01_len'], 'max' => $app['config']['zip01_len'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_zip02'], array(
            new Assert\NotBlank(),
            new Assert\Type(array('type' => 'numeric', 'message' => 'form.type.numeric.invalid')),
            new Assert\Length(array('min' => $app['config']['zip02_len'], 'max' => $app['config']['zip02_len'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_addr01'], array(
            new Assert\NotBlank(),
            new Assert\Length(array('max' => $app['config']['address1_len'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_addr02'], array(
            new Assert\NotBlank(),
            new Assert\Length(array('max' => $app['config']['address2_len'])),
        ));

        $errors[] = $app['validator']->validateValue($data['customer_email'], array(
            new Assert\NotBlank(),
            new Assert\Email(array('strict' => true)),
        ));

        return $errors;
    }
}
