<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\CustomEntryForm\Controller\Front\Block;

use Eccube\Application;
use Eccube\Common\Constant;

use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Eccube\Util\Str;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;

class CustomFooterBlockController extends \Plugin\CustomEntryForm\Controller\Front\CustomEntryFormController
{
    private $title;


    public function __construct()
    {
        $this->title = '';
    }


    public function index(Application $app, Request $request,$type)
    {

        // スマートフォン
        $qb = $app['orm.em']->getRepository('Eccube\Entity\Product')->createQueryBuilder('p')->andWhere('p.Status = 1');
        $qb
            ->innerJoin('p.ProductCategories', 'pct')
            ->innerJoin('pct.Category', 'c')
            ->andWhere($qb->expr()->in('pct.Category', ':Categories'))
            ->setParameter('Categories', 1);
        $qb->addOrderBy('p.name', 'DESC');
        $ProductList[1] = $qb->getQuery()->getResult();
        // dump($Product[1][0]);

        // 3Gケータイ
        $qb = $app['orm.em']->getRepository('Eccube\Entity\Product')->createQueryBuilder('p')->andWhere('p.Status = 1');
        $qb
            ->innerJoin('p.ProductCategories', 'pct')
            ->innerJoin('pct.Category', 'c')
            ->andWhere($qb->expr()->in('pct.Category', ':Categories'))
            ->setParameter('Categories', 2);
        $qb->addOrderBy('p.name', 'DESC');
        $ProductList[2] = $qb->getQuery()->getResult();
        // dump($result);

        // キャンペーン
        $CampaignList = $app['eccube.plugin.customurluserpage.repository.customurluserpage']->findList('campaign');
        $filterpg =array();

        foreach($CampaignList as $pg){
            $urlbind = true;
            $service = $app['eccube.plugin.customurluserpage.service.customurluserpage'];
            if($service){
                $urlbind = $service->CheckDisp($pg);
            }

            if($urlbind){

                $filterpg[] = $pg;

            }
        }



        if($type=='footer'){
        return $app['view']->render("Block/customfooter_block.twig", array(
            'ProductList' => $ProductList,
            'CampaignList' => $filterpg,
        ));

        }else{
        return $app['view']->render("Block/customsidelink_block.twig", array(
            'ProductList' => $ProductList,
            'CampaignList' => $filterpg,
        ));

        }




    }


}
