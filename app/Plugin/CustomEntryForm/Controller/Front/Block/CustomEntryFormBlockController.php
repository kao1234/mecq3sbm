<?php
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2015 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

namespace Plugin\CustomEntryForm\Controller\Front\Block;

use Eccube\Application;
use Eccube\Common\Constant;

use Eccube\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception as HttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Eccube\Util\Str;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Exception\CartException;

class CustomEntryFormBlockController extends \Plugin\CustomEntryForm\Controller\Front\CustomEntryFormController
{
    private $title;

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionKey = 'eccube.front.shopping.nonmember';

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionCustomerAddressKey = 'eccube.front.shopping.nonmember.customeraddress';

    /**
     * @var string 複数配送警告メッセージ
     */
    private $sessionMultipleKey = 'eccube.front.shopping.multiple';

    /**
     * @var string 受注IDキー
     */
    private $sessionOrderKey = 'eccube.front.shopping.order.id';


    public function __construct()
    {
        $this->title = '';
    }


    public function detail_custom(Application $app, Request $request,$id)
    {

        $id = $app['request']->attributes->get('id');

        if( !$id ){
            return;
        }

        $BaseInfo = $app['eccube.repository.base_info']->get();
        if ($BaseInfo->getNostockHidden() === Constant::ENABLED) {
            $app['orm.em']->getFilters()->enable('nostock_hidden');
        }

        /* @var $Product \Eccube\Entity\Product */
        $Product = $app['eccube.repository.product']->get($id);

        if (!$request->getSession()->has('_security_admin') && $Product->getStatus()->getId() !== 1) {
            throw new NotFoundHttpException();
        }
        if (count($Product->getProductClasses()) < 1) {
            throw new NotFoundHttpException();
        }

        $pcolors = $app['eccube.plugin.productcolor.repository.product_productcolor']->findProductColor($Product);

        /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
        $builder = $app['form.factory']->createNamedBuilder('', 'customentryform_add_cart', null, array(
            'product' => $Product,
            'id_add_product_id' => false,
        ));

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Product' => $Product,
            ),
            $request
        );
        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_INITIALIZE, $event);
        // $builder->remove('classcategory_id1');
        // $builder->remove('classcategory_id2');



        /* @var $form \Symfony\Component\Form\FormInterface */
        $form = $builder->getForm();

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $addCartData = $form->getData();
                if ($addCartData['mode'] === 'add_favorite') {
                    if ($app->isGranted('ROLE_USER')) {
                        $Customer = $app->user();
                        $app['eccube.repository.customer_favorite_product']->addFavorite($Customer, $Product);
                        $app['session']->getFlashBag()->set('product_detail.just_added_favorite', $Product->getId());

                        $event = new EventArgs(
                            array(
                                'form' => $form,
                                'Product' => $Product,
                            ),
                            $request
                        );
                        $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_FAVORITE, $event);

                        if ($event->getResponse() !== null) {
                            return $event->getResponse();
                        }

                        return $app->redirect($app->url('product_detail', array('id' => $Product->getId())));
                    } else {
                        // 非会員の場合、ログイン画面を表示
                        //  ログイン後の画面遷移先を設定
                        $app->setLoginTargetPath($app->url('product_detail', array('id' => $Product->getId())));
                        $app['session']->getFlashBag()->set('eccube.add.favorite', true);
                        return $app->redirect($app->url('mypage_login'));
                    }
                } elseif ($addCartData['mode'] === 'customentryform_add_cart') {

                    log_info('カート追加処理開始', array('product_id' => $Product->getId(), 'product_class_id' => $addCartData['product_class_id'], 'quantity' => $addCartData['quantity']));

                    try {
                        $app['eccube.service.cart']->addProduct($addCartData['product_class_id'], $addCartData['quantity'])->save();
                    } catch (CartException $e) {
                        log_info('カート追加エラー', array($e->getMessage()));
                        $app->addRequestError($e->getMessage());
                    }

                    log_info('カート追加処理完了', array('product_id' => $Product->getId(), 'product_class_id' => $addCartData['product_class_id'], 'quantity' => $addCartData['quantity']));

                    $event = new EventArgs(
                        array(
                            'form' => $form,
                            'Product' => $Product,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_COMPLETE, $event);

                    if ($event->getResponse() !== null) {
                        return $event->getResponse();
                    }

                    return $app->redirect($app->url('cart'));
                }
            }
        } else {
            $addFavorite = $app['session']->getFlashBag()->get('eccube.add.favorite');
            if (!empty($addFavorite)) {
                // お気に入り登録時にログインされていない場合、ログイン後にお気に入り追加処理を行う
                if ($app->isGranted('ROLE_USER')) {
                    $Customer = $app->user();
                    $app['eccube.repository.customer_favorite_product']->addFavorite($Customer, $Product);
                    $app['session']->getFlashBag()->set('product_detail.just_added_favorite', $Product->getId());
                }
            }
        }

        /* @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $app['orm.em']->getRepository('Plugin\TagEx\\Entity\TagEx')->createQueryBuilder('tagex');

        $qb
            ->innerjoin('tagex.Tag','tag')
            ->innerjoin('tag.ProductTag','pt')
            ->innerjoin('pt.Product','p')
            ->AndWhere('p.id=:product_id')
            ->setParameter(':product_id', $Product->getId())
            ->orderBy('tag.rank', 'desc');

            // ->innerJoin('pt.Tag', 'tag')
            // ->andWhere('p.id = :product_id')
            // ->setParameter(':product_id', $Product->getId())
            // ->orderBy('tag.rank', 'desc');

        $ProductTags = $qb->getQuery()->getResult();

        $is_favorite = false;
        if ($app->isGranted('ROLE_USER')) {
            $Customer = $app->user();
            $is_favorite = $app['eccube.repository.customer_favorite_product']->isFavorite($Customer, $Product);
        }
        return $app['view']->render("Block/customentryform_block.twig", array(
            'title' => $this->title,
            'subtitle' => $Product->getName(),
            'form' => $form->createView(),
            'Product' => $Product,
            'is_favorite' => $is_favorite,
            'ProductColor'=>$pcolors,
            'ProductTags'=>$ProductTags,
            'ProductType'=>$Product['ProductClasses'][0]['ProductType']['id'],
        ));
    }

    public function detail_custom2(Application $app, Request $request, $id)
    {


        $id = $app['request']->attributes->get('id');

        if( !$id ){
            return;
        }


        $BaseInfo = $app['eccube.repository.base_info']->get();
        if ($BaseInfo->getNostockHidden() === Constant::ENABLED) {
            $app['orm.em']->getFilters()->enable('nostock_hidden');
        }

        /* @var $Product \Eccube\Entity\Product */
        $Product = $app['eccube.repository.product']->get($id);

        if (!$request->getSession()->has('_security_admin') && $Product->getStatus()->getId() !== 1) {
            throw new NotFoundHttpException();
        }
        if (count($Product->getProductClasses()) < 1) {
            throw new NotFoundHttpException();
        }
        $pcolors = $app['eccube.plugin.productcolor.repository.product_productcolor']->findProductColor($Product);



        /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
        $builder = $app['form.factory']->createNamedBuilder('', 'customentryform_add_cart', null, array(
            'product' => $Product,
            'id_add_product_id' => false,
        ));

        $event = new EventArgs(
            array(
                'builder' => $builder,
                'Product' => $Product,
            ),
            $request
        );
        // $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_INITIALIZE, $event);
        // $builder->remove('classcategory_id1');
        // $builder->remove('classcategory_id2');



        /* @var $form \Symfony\Component\Form\FormInterface */
        $form = $builder->getForm();

        /*
        //処理されないので、コメント

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $addCartData = $form->getData();

                if ($addCartData['mode'] === 'add_favorite') {
                    // if ($app->isGranted('ROLE_USER')) {
                    //     $Customer = $app->user();
                    //     $app['eccube.repository.customer_favorite_product']->addFavorite($Customer, $Product);
                    //     $app['session']->getFlashBag()->set('product_detail.just_added_favorite', $Product->getId());

                    //     $event = new EventArgs(
                    //         array(
                    //             'form' => $form,
                    //             'Product' => $Product,
                    //         ),
                    //         $request
                    //     );
                    //     $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_FAVORITE, $event);

                    //     if ($event->getResponse() !== null) {
                    //         return $event->getResponse();
                    //     }

                    //     return $app->redirect($app->url('product_detail', array('id' => $Product->getId())));
                    // } else {
                    //     // 非会員の場合、ログイン画面を表示
                    //     //  ログイン後の画面遷移先を設定
                    //     $app->setLoginTargetPath($app->url('product_detail', array('id' => $Product->getId())));
                    //     $app['session']->getFlashBag()->set('eccube.add.favorite', true);
                    //     return $app->redirect($app->url('mypage_login'));
                    // }
                } elseif (in_array($addCartData['mode'],array('customentryform_add_cart','customentryform_add_cart_shop'))
                    ) {
                    $formtype = array('customentryform_add_cart'=>'web','customentryform_add_cart_shop'=>'shop');



                    log_info('カート追加処理開始', array('product_id' => $Product->getId(), 'product_class_id' => $addCartData['product_class_id'], 'quantity' => $addCartData['quantity']));

                    try {
                        $Cart = $app['eccube.service.cart']->getCart();

                        if($Cart){
                            $removed=false;
                            foreach($Cart->getCartItems() as $CartItem){

                                $ProductClass = $CartItem->getObject();
                                if ($ProductClass) {

                                    $app['eccube.service.cart']->removeProduct($ProductClass->getId());
                                    $removed=true;
                                }
                            }
                            if($removed){
                                $app['eccube.service.cart']->save();

                            }

                        }


                        $app['eccube.service.cart']->addProduct($addCartData['product_class_id'], $addCartData['quantity'],$addCartData)->save();
                    } catch (CartException $e) {
                        log_info('カート追加エラー', array($e->getMessage()));
                        $app->addRequestError($e->getMessage());
                    }

                    log_info('カート追加処理完了', array('product_id' => $Product->getId(), 'product_class_id' => $addCartData['product_class_id'], 'quantity' => $addCartData['quantity']));

                    $event = new EventArgs(
                        array(
                            'form' => $form,
                            'Product' => $Product,
                        ),
                        $request
                    );
                    $app['eccube.event.dispatcher']->dispatch(EccubeEvents::FRONT_PRODUCT_DETAIL_COMPLETE, $event);

                    if ($event->getResponse() !== null) {
                        return $event->getResponse();
                    }

                    return $app->redirect($app->url('plugin_customentryform_form'.$formtype[$addCartData['mode']].'entry',array('id'=>$id)));
                }
            }
        } else {
            $addFavorite = $app['session']->getFlashBag()->get('eccube.add.favorite');
            if (!empty($addFavorite)) {
                // お気に入り登録時にログインされていない場合、ログイン後にお気に入り追加処理を行う
                if ($app->isGranted('ROLE_USER')) {
                    $Customer = $app->user();
                    $app['eccube.repository.customer_favorite_product']->addFavorite($Customer, $Product);
                    $app['session']->getFlashBag()->set('product_detail.just_added_favorite', $Product->getId());
                }
            }
        }

        $is_favorite = false;
        if ($app->isGranted('ROLE_USER')) {
            $Customer = $app->user();
            $is_favorite = $app['eccube.repository.customer_favorite_product']->isFavorite($Customer, $Product);
        }
        */
        /* @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $app['orm.em']->getRepository('Plugin\TagEx\\Entity\TagEx')->createQueryBuilder('tagex');

        $qb
            ->innerjoin('tagex.Tag','tag')
            ->innerjoin('tag.ProductTag','pt')
            ->innerjoin('pt.Product','p')
            ->AndWhere('p.id=:product_id')
            ->setParameter(':product_id', $Product->getId())
            ->orderBy('tag.rank', 'desc');

            // ->innerJoin('pt.Tag', 'tag')
            // ->andWhere('p.id = :product_id')
            // ->setParameter(':product_id', $Product->getId())
            // ->orderBy('tag.rank', 'desc');

        $ProductTags = $qb->getQuery()->getResult();


        return $app['view']->render("Block/customentryform_side_block.twig", array(
            'title' => $this->title,
            'subtitle' => $Product->getName(),
            'form' => $form->createView(),
            'Product' => $Product,
            'is_favorite' => false,
            'ProductColor'=>$pcolors,
            'ProductTags'=>$ProductTags,
        ));

        // return $app->render('Product/detail.twig', array(
        //     'title' => $this->title,
        //     'subtitle' => $Product->getName(),
        //     'form' => $form->createView(),
        //     'form2' => $form->createView(),
        //     'Product' => $Product,
        //     'is_favorite' => $is_favorite,
        // ));
    }

}
