<?php
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2016 LOCKON CO.,LTD. All Rights Reserved.
* http://www.lockon.co.jp/
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Plugin\CustomEntryForm;

use Eccube\Application;
use Eccube\Event\EventArgs;
use Eccube\Event\TemplateEvent;

/**
 * プラグインイベント処理ルーティングクラス
 * Class CustomEntryForm
 * @package Plugin\CustomEntryForm
 */
class CustomEntryFormEvent
{
    /**
     * @var string 非会員用セッションキー
     */
    private $sessionKey = 'eccube.front.shopping.nonmember';

    /**
     * @var string 非会員用セッションキー
     */
    private $sessionCustomerAddressKey = 'eccube.front.shopping.nonmember.customeraddress';

    /**
     * @var string 受注IDキー
     */
    private $sessionOrderKey = 'eccube.front.shopping.order.id';


    /**
     * プラグインが追加するフォーム名
     */
    const SHOPPINGEX_TEXTAREA_NAME = 'cardno';

    /**
     * セッションに保存するテンポラリキー
     */
    const SHOPPINGEX_SESSON_ORDER_KEY = 'eccube.plugin.shoppingex.order.key';
    const SHOPPINGEX_SESSION_KEY = 'eccube.plugin.shoppingex.cardinfovalue.key';
    const SHOPPINGEX_SESSION_REDIRECT_KEY = 'eccube.plugin.shoppingex.redirect.key';

    const SHOPPINGEX_CREDIT_ORDER_TYPE_ID = "5";    //クレカ
    const SHOPPINGEX_SELFPAY_ORDER_TYPE_ID = "4";   //代引き

    const SHOPPINGEX_PAYMONTHLY_PRODUCTCLASS_ID = "9";   //月額払い
    const SHOPPINGEX_PAYONCE_PRODUCTCLASS_ID = "10";   //一括払い


    /** @var  \Eccube\Application $app */
    protected $app;
    /** @var \Eccube\Entity\BaseInfo */
    public $BaseInfo;

    /**
     * Event constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->BaseInfo = $app['eccube.repository.base_info']->get();
    }

    /**
     * フロント画面権限確認
     *
     * @return bool
     */
    protected function isAuthRouteFront()
    {
        return $this->app->isGranted('ROLE_USER');
    }
    public function onRenderAdminProductIndex(TemplateEvent $event){

    }

    public function onRenderAdminOrderIndex(TemplateEvent $event){

    }
    public function onRenderAdminOrderEdit(TemplateEvent $event){
    }


    public function onRenderFormComplete(TemplateEvent $event){
        $parameters = $event->getParameters();

        $tagorder = $this->app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->getA8TagOrderCode();

        if($tagorder){

            $parameters['a8ordercode']="s".$tagorder;
            $event->setParameters($parameters);
            $this->app['eccube.plugin.CustomEntryForm.service.CustomEntryForm']->removeTagOrderKey();

        }

    }

    public function onFrontShoppingIndexInitialize(EventArgs $event){


    }

    public function onFrontShoppingConfirmInitialize(EventArgs $event){
        $app=$this->app;
        $req=$event->getRequest();
        $sec = $req->getSession();
        $Order = $event->getArgument('Order');


        $builder = $event->getArgument('builder');
        $form = $builder->getForm();

        $request = $event->getRequest();
        $form->handleRequest($request);

        if (!$form->isValid()) {

            $Order = $event->getArgument('Order');
            $id = $event->getArgument('id');
            $formtype = $event->getArgument('formtype');

            $data = $form->getData();
            $payment = $data['payment'];
            $message = $data['message'];

            $Order->setPayment($payment);
            $Order->setPaymentMethod($payment->getMethod());
            $Order->setMessage($message);
            $Order->setCharge($payment->getCharge());

            // 合計金額の再計算
            $Order = $app['eccube.service.shopping']->getAmount($Order);

            // 受注関連情報を最新状態に更新
            $app['orm.em']->flush();

            // dump('confirm redirect');
            $session = $request->getSession();
            $session->set(self::SHOPPINGEX_SESSION_REDIRECT_KEY,$request->request);
            $event->setResponse($app->redirect(
                $app->url('plugin_customentryform_form'.$formtype.'_confirm',array('id'=>$id))
                ));

            //$app->addError('form.invalid.exception', 'admin');

        }else{

        }

    }
    public function onFrontShoppingConfirmProcessing(EventArgs $event){


        $app = $this->app;
        $Order = $event->getArgument('Order');
        // $this->setCustomDeliveryFee($Order,false);

        $form = $event->getArgument('form');
        $dat = $form->GetData();

        // $ShoppingEx = $app['shoppingex.repository.shoppingex']->find($Order->getId());
        // if(is_null($ShoppingEx)){
        //     $ShoppingEx = new ShoppingEx();

        // }
        // if(isset($dat['cardno1'])
        //     ){
        //     $ShoppingEx
        //             ->setId($Order->getId())
        //             ->setCardno1($dat['cardno1'])
        //             // ->setCardno2($dat['cardno2'])
        //             // ->setCardno3($dat['cardno3'])
        //             // ->setCardno4($dat['cardno4'])
        //             ->setHolder($dat['holder'])
        //             ->setCardtype($dat['cardtype'])
        //             ->setCardlimitmon($dat['cardlimitmon'])
        //             ->setCardlimityear($dat['cardlimityear'])
        //             ->setCardsec($dat['cardsec'])
        //             ;

        // }else{
        //     $ShoppingEx
        //             ->setId($Order->getId())
        //             ->setCardno1('')
        //             // ->setCardno2($dat['cardno2'])
        //             // ->setCardno3($dat['cardno3'])
        //             // ->setCardno4($dat['cardno4'])
        //             ->setHolder('')
        //             ->setCardtype(0)
        //             ->setCardlimitmon(0)
        //             ->setCardlimityear(0)
        //             ->setCardsec('')
        //             ;

        // }
        // $app['orm.em']->persist($ShoppingEx);
        // $app['orm.em']->flush();

        // if($Order->getPayment()->getId()==self::SHOPPINGEX_CREDIT_ORDER_TYPE_ID){
        // //クレカ決裁の場合、出力用の情報を引数に入れる
        //     $cardtypearr = explode(",",$app['config']['cardtype']);

        //     $event->setArgument('CardInfo',
        //         array(
        //             'cardno'=>$dat['cardno1'],
        //             //.$dat['cardno2'].$dat['cardno3'].$dat['cardno4'],
        //             'cardholder'=>$dat['holder'],
        //             'cardtype'=>$cardtypearr[$dat['cardtype']],
        //             'cardsec'=>$dat['cardsec'],
        //             'cardlimitmon'=>$dat['cardlimitmon'],
        //             'cardlimityear'=>$dat['cardlimityear']

        //         )
        //     );
        // }else{
            $event->setArgument('CardInfo',null);

        // }

        $event->getRequest()
            ->getSession()
            ->set(
                self::SHOPPINGEX_SESSION_KEY,
                $event->getArgument('CardInfo')
                 );

        // $app['eccube.plugin.shoppingex.service.shoppingex']->sendShoppingOrder($event);


    }

    public function onFrontShoppingConfirmComplete(EventArgs $event){

        $app=$this->app;
        $req=$event->getRequest();
        $sec = $req->getSession();
        $Order = $event->getArgument('Order');
        // $email = $form['email']->getData();

        $app = $this->app;
        //セッションから消す
        //$session->set(self::SHOPPINGEX_SESSION_REDIRECT_KEY,$request->request);
        $req = $event->getRequest();
        $sec = $req->getSession();
        if($sec->get(self::SHOPPINGEX_SESSION_REDIRECT_KEY)){
            $sec->remove(self::SHOPPINGEX_SESSION_REDIRECT_KEY);
        }

        $app['eccube.plugin.shoppingex.service.shoppingex']->cleanupShoppingOrder($event);


    }
    public function onFrontShoppingCompleteInitialize(EventArgs $event){
        $app = $this->app;


    }    
    public function onFrontShoppingPaymentInitialize(EventArgs $event){


    }

    public function onFrontShoppingPaymentComplete(EventArgs $event){



    }



    public function onFrontShoppingDeliveryInitialize(EventArgs $event){



    }
    public function onFrontShoppingDeliveryComplete(EventArgs $event){

    }
    public function onFrontShoppingShippingChangeInitialize(EventArgs $event){



    }
    public function onFrontShoppingShippingComplete(EventArgs $event){


    }
    public function onFrontShoppingShippingEditChangeInitialize(EventArgs $event){

    }
    public function onFrontShoppingShippingEditInitialize(EventArgs $event){



    }
    public function onFrontShoppingShippingEditComplete(EventArgs $event){

    }



    public function onFrontShoppingNonmemberInitialize(EventArgs $event){


    }
    public function onFrontShoppingNonmemberComplete(EventArgs $event){

        $app=$this->app;



    }
    public function onRenderShoppingNonMember(TemplateEvent $event){



    }



    public function onFrontShoppingShippingMultipleChangeInitialize(EventArgs $event){



    }
    public function onFrontShoppingShippingMultipleInitialize(EventArgs $event){


    }
    public function onFrontShoppingShippingMultipleComplete(EventArgs $event){

    }
    public function onFrontShoppingShippingMultipleEditInitialize(EventArgs $event){


    }
    public function onFrontShoppingShippingMultipleEditComplete(EventArgs $event){

    }

    /*
    メール文面に情報を差し込む処理
    プラグインの処理を上書き
    */
    public function onMailServiceMailOrder(EventArgs $event){

        $app = $this->app;
        

        $MailTemplate = $event['MailTemplate'];
        $Order = $event['Order'];
        $message = $event['message'];
        
        if($this->ismobareco()){

            $MailTemplate = $this->app['eccube.repository.mail_template']
                                 ->find( $this->app['config']['CustomEntryForm']['const']['setting']['mobareco_mail_template']);
            $message
            ->setSubject('[' . $this->BaseInfo->getShopName() . '] ' . $MailTemplate->getSubject())
            ;
        }        

        $orderDetails = $Order->getOrderDetails();
        //$plgOrderDetails = $app['eccube.productoption.service.util']->getPlgOrderDetails($orderDetails);
        
        $Shippings = $Order->getShippings();
        //$plgShipmentItems = $app['eccube.productoption.service.util']->getPlgShipmentItems($Shippings);
        

        $CardInfo = null;

        $body = $app->renderView('Mail/order.twig', array(
            'header' => $MailTemplate->getHeader(),
            'footer' => $MailTemplate->getFooter(),
            'Order' => $Order,
            'Card' => $CardInfo,
            'plgOrderDetails' => $plgOrderDetails,
            'plgShipmentItems' => $plgShipmentItems,
        ));
        
        $message->setBody($body);
        
        $event['message'] = $message;

        $app['request']->getSession()
                       ->remove(self::SHOPPINGEX_SESSION_KEY);

        // dump($event);//die();

    }
    /*
    メール文面に情報を差し込む処理
    プラグインの処理を上書き
    */
    public function onMailServiceMailContact(EventArgs $event){

        $app = $this->app;
        
        $MailTemplate = $event['MailTemplate'];
        $formData = $event['formData'];
        $message = $event['message'];
        
        if($this->ismobareco()){

            $MailTemplate = $this->app['eccube.repository.mail_template']
                                 ->find( $this->app['config']['CustomEntryForm']['const']['setting']['mobareco_mail_contact_template']);
            $message
            ->setSubject('[' . $this->BaseInfo->getShopName() . '] ' . $MailTemplate->getSubject())
            ;
        }        


        $body = $this->app->renderView('Mail/contact_mail.twig', array(
            'header' => $MailTemplate->getHeader(),
            'footer' => $MailTemplate->getFooter(),
            'data' => $formData,
            'BaseInfo' => $this->BaseInfo,
        ));
        
        $message->setBody($body);
        
        $event['message'] = $message;


    }    
    private function ismobareco(){
        $service = $this->app['eccube.plugin.fdroute.service.fdroute'];
        if($service){
            return $service->isMobareco();

        }else{
            return false;
        }

    }


    public function onFrontContactIndexComplete(EventArgs $event){
        $app = $this->app;


    }
    public function onFrontProductDetailInitialize(EventArgs $event){
        $app = $this->app;


    }
    public function onFrontProductDetailComplete(EventArgs $event){
        $app = $this->app;


    }
    public function onRenderProductDetail(TemplateEvent $event){

    }



    public function onFrontCartIndexInitialize(EventArgs $event){
        $app = $this->app;




    }
    public function onFrontCartIndexComplete(EventArgs $event){
        $app = $this->app;



    }

    public function onRenderCart(TemplateEvent $event){

    }


    public function onFrontCartAddInitialize(EventArgs $event){
        $app = $this->app;


    }
    public function onFrontCartAddComplete(EventArgs $event){
        $app = $this->app;


    }
    public function onFrontCartAddException(EventArgs $event){
        $app = $this->app;



    }

    public function onRenderMyPageIndex(TemplateEvent $event){
        $app = $this->app;


    }


}
