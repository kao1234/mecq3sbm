

$(document).ready(function(){


	jQuery(function($) {
		var nav = $('#header'),
		offset = nav.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				nav.addClass('fixed');
			} else {
				nav.removeClass('fixed');
			}
		});
	});


	$(".drawer2").drawer();


  $(window).load(function(){

     $('.foot_nav_box').matchHeight();


  });


	//Debaug
	var DD = ["HtmlImg","Html"];
	//	DD = ["Bottom","Html"];
	//	DD = ["Html","Img"];
	//	$('#wrapper').TraceCoder({getimage : true ,mode : DD});

    // $("#hover").GnavUp({'btn' :'.hover_btn'});

//.top_bnr_box
//.header

	$('a[href^="#"]').click(function(){
        var hhh = 0;
        if($(".details_menu_fixed").is('*')){
            hhh += $(".details_menu_fixed").outerHeight();
            if($(window).scrollTop() <= 32){
                hhh += $(".details_menu_fixed").outerHeight();
            }
        }
        if($("#header").is('*')){
            hhh += $("#header").outerHeight();
        }
        if(!$(".details_menu_fixed").is('*') && $("#header").is('*')){
            if($(window).scrollTop() <= 32){
                hhh += $("#header").outerHeight();
            }
        }


		var strID = $(this).attr('href');
		var objBlock = $(strID);
		if(objBlock.size()>0){
			$('body,html').animate({scrollTop: (objBlock.offset().top - hhh - 10) },700,'swing');
		}
		return false;
	});



    $('.top_bnr_btn').click(function(){
        $(".top_bnr_box").css({"display":"none"});
        return false;
    });



  var foot        = $('#footer');
  var side_bnr    = $('.side_bnr');
  var main_r      = $('.main_r');
  var main_l      = $('.main_l');
//  $(".main_r").wrap('<div class="main_fix"><div class="main_fix_inner"></div></div>');


    $(window).on('load resize',function(){

      var foot_offset = foot.offset();
      side_bnr.css({"height":(foot_offset.top + foot.outerHeight())});
      main_l.css({"min-height": (main_r.outerHeight() + 10) });

    });





});


;(function($){


	/***********************

	TraceCoder

	*/
$.fn.TraceCoder = function(options){var defaults = {image : 'sample.png',getimage : false, mode : ["HtmlImg","Html","Img","Bottom"], backbg : 'body'
}
var options = $.extend(defaults, options); var base = this; var element = $(this); var elementid = element.attr('id'); var backbg = $(options.backbg); var click_type = 0; $('body').append($('<div id="'+elementid+'CodingDebugBtn" style="position:fixed;top:10px;right:10px;z-index:99999;background:#999;color:#fff;padding:5px;text-align:center;"><a href="aaa" style="color:#fff;" >'+options.mode[click_type]+'</a></div>')); if(options.getimage){ options.image = GetFileName();}
ChangeBG(options.mode[click_type]); $('#'+elementid+'CodingDebugBtn').click(function(){ click_type++; if(click_type > options.mode.length - 1 ) click_type=0; $('#'+elementid+'CodingDebugBtn a').html(options.mode[click_type]); ChangeBG(options.mode[click_type]); return false;}); function ChangeBG(type){ var url = ""; if(options.image){ url = "url(" + (options.image.replace(".html", ".").replace(".php", ".") + "png") + ")";}else{ url = "url(index.png)";}
if(type == "HtmlImg"){ element.css('opacity',0.5); backbg.css({'background-repeat':'no-repeat','background-image': url });}else if(type == "Html"){ element.css('opacity',1); backbg.css({'background-image': "none" });}else if(type == "Img"){ element.css('opacity',0); backbg.css({'background-repeat':'no-repeat','background-image': url });}else if(type == "Bottom"){ element.css('opacity',0.5); backbg.css({'background-position':'50% 100%','background-repeat':'no-repeat','background-image': url });}
}
return this;}




    /***********************

    BackResaize

    */
    $.fn.BackResaize = function(options) {
        var defaults = {
            bg_w: '100',
            bg_h: '100',
            bg_class: "bg_Class"
        }
        var options = $.extend(defaults, options);
        var base = this;
        var element = $(this);
        var elementid = element.attr('id');


        var h = options.bg_h;
        var w = options.bg_w;

		$(window).load(function() {
				back_resaize();
		});


	    $(window).resize(function() {
	    	back_resaize();
	    });

		function back_resaize(){

	    	var window_w = element.outerWidth();
	    	var window_h = element.outerHeight();

	        if (window_w < w && window_h < h) {
	            if (w > window_w) { h = (window_w * h) / w; w = window_w; }
	            if (h > window_h) { w = (window_h * w) / h; h = window_h; }
	        }
	        if (w < window_w) { h = (window_w * h) / w; w = window_w; }
	        if (h < window_h) { w = (window_h * w) / h; h = window_h; }

	        x = y = 0;

	        if (w > window_w) { x = ((w - window_w) - ((w - window_w) % 2)) / 2; }
	        if (h > window_h) { y = ((h - window_h) - ((h - window_h) % 2)) / 2; }

	    	$("#"+elementid+" "+options.bg_class).css({"width":w,"height":h,"top":(y * -1),"left":(x * -1)});

		}

        return this;
    }



    /***********************
    GnavUp
    */

    $.fn.GnavUp = function(options){
        var defaults = {
            easing  : 'swing',
            btn     : '.gnav_sp_btn',
            onClick : function() {}

        }
        var options     = $.extend(defaults, options);
        var base             = this;
        var element          = $(this);

        GnavUpAnim();

        function GnavUpAnim(){

            //スマホポップアップ
            var gnav_sp_btn_flag = false;
            var gnav_sp_btn_click = true;
            var currentPos = 0;

            $(options.btn).click(function () {


                if(gnav_sp_btn_click){
                    gnav_sp_btn_click = false;


                if(!gnav_sp_btn_flag){

                    currentPos = $(window).scrollTop();
                    $("body").css({"top": (currentPos * -1) });


                      element.css({"opacity": 0 ,"display": "block" });
                      options.onClick();
                      element.animate({"opacity": 1 },{
                        duration: 800,easing: options.easing,
                        'complete': function(){

                            gnav_sp_btn_flag = true;
                            gnav_sp_btn_click = true;
                        }
                      });
                }else{
                    $("body").css({"top": "" });
                    $('body,html').animate({scrollTop:currentPos},1,'swing');


                      element.animate({"opacity": 0 },{
                        duration: 800,easing: options.easing,
                        'complete': function(){
                              element.css({"display": "none" });

                            gnav_sp_btn_flag = false;
                            gnav_sp_btn_click = true;
                        }
                      });
                }
                //クラスがなければ追加、あれば削除
                    $("html").toggleClass("menu-open");
                }

                if($(this).data("return") == "on"){
                }else{
                    return false;
                }

            });
        }
        return base;
    }


	/***********************
	Screen
	*/

	$.fn.Screen = function(options){
		var defaults = {
			aaa  : ''
		}
		var options     = $.extend(defaults, options);
		var base             = this;
		var element          = $(this);

	    $('<div id="screen-check"></div>').appendTo(base);

		this.GetScreen = function(){
			screen_check = $('#screen-check').css('color');
			screen_size = {tablet:false,sp:false,portrait:false,pc:false};
			if(screen_check == "rgb(0, 0, 255)"){ screen_size.portrait = true; }
			else if(screen_check == "rgb(0, 255, 0)"){ screen_size.sp = true;}
			else if(screen_check == "rgb(255, 0, 0)"){ screen_size.tablet = true;}
			else{ screen_size.pc = true; }
			return screen_size;
		}
		return base;
	}



})(jQuery);



/* filename */
function GetFileName()
{
    var last = document.URL.length;
    var findS = document.URL.lastIndexOf('/');
    var findE = document.URL.lastIndexOf('\\');
    var start = 0;
    if((findS > -1) || (findE > -1))
    {
        start = (findS < findE) ? findE : findS;
        start = start + 1;
    }
    return document.URL.substring(start, last);
}


