$(document).ready(function(){


  $(window).load(function(){

     $('.list_item_name').matchHeight();
     $('.list_item_img').matchHeight();
     $('.list_item_mes').matchHeight();
     $('.list_item_m').matchHeight();
     $('.list_item_btn').matchHeight();
     $('.list_item li').matchHeight();

  });


 //ループ
  $('.sort li').each(function(key) {
	  //クリック
	  $(this).children("a").click(function () {

	  		$('.sort li a').removeClass("sort_on");
	  		$(this).addClass("sort_on");

			$('.list_item_body ul').removeClass("list_item_on");
			$('.list_item_body ul:eq('+key+')').addClass("list_item_on");

	    return false;
	  });
  });

});