$(document).ready(function(){
	
	jQuery(function($) {
		var nav = $('#header'),
		offset = nav.offset();
		$(window).scroll(function () {
			if($(window).scrollTop() > offset.top) {
				$('.details_menu_fixed').addClass('fixed');
			} else {
				$('.details_menu_fixed').removeClass('fixed');
			}
		});
	});
	
	$('.bxslider').bxSlider({
		speed: 1000,
		pause: 4000,
		auto:true,
		mode: 'horizontal',
		pager: true,
		controls: true,
	});


  $(window).load(function(){

      $('.detailsData_list li').matchHeight();

  });



	  //ループ
	$(".details_productInfo_color_list label").each(function(key) {
		//クリック
		$(this).click(function () {
			$(this).parent().children("label").removeClass("label_on");
			$(this).addClass("label_on");
		});
  }); 

	  //ループ
	$(".details_productInfo_m_list label").each(function(key) {

		//クリック
		$(this).click(function () {
			$(this).parent().parent().find("label").removeClass("label_on");
			$(this).addClass("label_on");
		});


  }); 



});